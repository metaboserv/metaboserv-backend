FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive

RUN     apt-get update
RUN     apt-get upgrade -y
RUN     apt-get install -y --no-install-recommends build-essential r-base libcurl4 openssl libmagic1

WORKDIR /app

RUN Rscript -e "install.packages('mrbin')"

RUN	apt-get install -y --no-install-recommends python3.7 python3-pip python3-setuptools python3-dev

COPY ./requirements.txt /app/requirements.txt
RUN     pip install -r /app/requirements.txt

RUN	apt-get install -y iputils-ping

VOLUME /app/uploads /app/normal_concentrations app/sourcefiles
RUN mkdir /app/plots
RUN mkdir /app/data
RUN mkdir /app/elasticsearch


COPY ./metaboserv_conf_docker.yml /app/metaboserv_conf_docker.yml
COPY ./src /app/src

ENV PYTHONPATH /app

EXPOSE 9201
ENTRYPOINT ["python3", "/app/src/metaboserv_api.py", "--docker", "--cache"]
