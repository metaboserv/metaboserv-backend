# MetaboSERV API Service

Adapter for metabolomics data (primarily NMR) to be stored in an ElasticSearch instance.

## Usage

A Python3 environment (e.g. 3.10.6) with the following packages has to be present:
* elasticsearch
* flask
* flask_cors
* flask_swagger_ui
* flask_jwt_extended
* pandas
* numpy
* werkzeug
* xmltodict
* yaml
* openpyxl
* pyyaml
* matplotlib
* seaborn

Furthermore, an ElasticSearch instance has to be accessible (e.g. on a server). Username, password, port etc. can be configured in `metaboserv_conf.yml`, or `metaboserv_conf_docker.yml` for the docker version.

To start the backend service, call `python src/metaboserv_api.py`. `--cache` can be passed as an argument to create a concentration cache on startup.
To run the backend service in a dedicated docker container, also pass the flag `--docker`. Otherwise it will try to run the service natively.

Swagger UI can be found at `{link}:{port}/{prefix}/doc`.

## Configuration

There are two configuration files, one for docker and one for running natively.
The service automatically detects if docker is used and utilizes the according configuration.

Configuration parameters can be found in the config file:
* **flask.host**: The IP Flask is running on
* **flask.port**: The port Flask is running on
* **flask.path**: A prefix that is automatically added to any Flask endpoint (defaults to /metaboserv-api/v1/)
* **flask.app_name**: Name of the app (shown in the browser)
* **flask.source_folder**: Folder where source files (i.e. unprocessed data like CSV files) are stored
* **flask.jwt_key**: Seed used for generating JWTs
* **flask.jwt_password**: Password that will automatically be set for the admin account
* **flask.jwt_https**: If true, will only accept JWTs that were sent over HTTPS 
* **flask.jwt_expiration**: Integer that defines for how many hours a JWT is considered valid (default: 3)
* **database.data_folder**: Folder where generated data will be stored
* **database.ontology_folder**: Folder on the machine where HMDB ontology data can be found
* **database.ontology_serum**: Filename for HMDB ontology serum data
* **database.plot_folder**: Folder for storing created plots (not currently used)
* **database.username**: Username for ES
* **database.password**: Password for ES
* **database.path**: Link where Python will look for the ES instance (not contained in docker config)
* **database.port**: Port where Python will look for the ES instance
* **database.certs_path**: Folder where ES stores its certificate
* **database.certs_name**: Name of the certificate file for ES
* **database.spectra_path**: Folder where raw spectra are stored
* **filestorage.upload_folder**: Upload folder for file upload
* **filestorage.mb_limit**: Maximum size in MB for file upload
* **filestorage.extensions**: Allowed extensions for file upload

Furthermore, the five Elasticsearch indices can be given different names.
This will not change any functionality, but may be preferred as they are very generic.

## Endpoints

For more information on the endpoints, check out the according SwaggerUI.

