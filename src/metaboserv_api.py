from flask import Flask, Response, request, send_from_directory, jsonify
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from flask_jwt_extended import jwt_required, create_access_token, JWTManager, current_user, verify_jwt_in_request
from flask_jwt_extended import get_jwt, get_jwt_identity, set_access_cookies, unset_jwt_cookies, get_csrf_token
from metaboserv_database import MetaboservDatabase
from metaboserv_helpers import KNOWN_SYNONYMS, ResultObject, createErrorResponse, createResponse, read_config, dict2obj
from functools import wraps

import os, copy
import logging
import json
import datetime
import argparse
import tarfile

from metaboserv_helpers import handleFileUpload, handleFileDeletion

# Configuration is read from here
CONFIG_PATH = 'metaboserv_conf.yml'
DOCKER_CONFIG_PATH = '/app/metaboserv_conf_docker.yml'

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Control the MetaboSERV backend service.')
    parser.add_argument("--cache", help="Create a concentration data cache on startup", action="store_true")
    parser.add_argument("--hmdb", help="Parse HMDB data and add it to the database", action="store_true")
    parser.add_argument("--docker", help="Set this flag if the backend is running in a docker container.", action="store_true")
    args = parser.parse_args()
    
    config = read_config(DOCKER_CONFIG_PATH) if args.docker else read_config(CONFIG_PATH)
    database_config = config['database']

    flask_config = config['flask']
    filestorage_config = config['filestorage']
    api_path = flask_config['path']
    api_host = flask_config['host']
    api_port = flask_config['port']
    sourcefile_path = flask_config['source_folder']

    host={"host": "metaboserv-es", "scheme": "http", "port": database_config['port']} if args.docker else database_config['path'] + ":" + str(database_config['port'])
    docker_bool = True if args.docker else False

    # Create Flask Object
    app = Flask(__name__, template_folder="templates", static_folder='static')
    CORS(app)
    app.config['MAX_CONTENT_LENGTH'] = 1024*1024 * \
        int(filestorage_config["mb_limit"])  # 50MB by default
    app.config['UPLOAD_EXTENSIONS'] = filestorage_config["extensions"]
    app.config['UPLOAD_FOLDER'] = filestorage_config['upload_folder']
    logging.basicConfig(level=logging.INFO)

    # Create database connection
    metaboserv = MetaboservDatabase(database_config, host, docker_bool)

    # Initialize JWT Manager
    jwt = JWTManager(app)
    app.config["JWT_SECRET_KEY"] = flask_config['jwt_key']
    app.config["JWT_TOKEN_LOCATION"] = ["headers", "cookies"]
    app.config["JWT_COOKIE_SECURE"] = flask_config['jwt_https'] # This should be True for production!
    app.config["JWT_ACCESS_TOKEN_EXPIRES"] = datetime.timedelta(hours=int(flask_config['jwt_expiration']))
    app.config["JWT_COOKIE_CSRF_PROTECT"] = True
    app.config["JWT_CSRF_IN_COOKIES"] = True
    
    # Admin account from yaml configuration
    admin_pw = flask_config['jwt_password']

    # Set up SwaggerUI
    SWAGGER_URL = f'{api_path}doc'
    API_URL = '/static/openapi.json'
    SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={
            'app_name': flask_config['app_name']
        }
    )
    app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

    if args.cache:
        metaboserv.createInitialCache()

    if args.hmdb:
        pass
        #metaboserv.parseFullMetaboliteOntology()
        #.readParsedMetaboliteOntology()

    ################### Auth Decorators ###################

    def admin_required():
        """
        Creates a decorator that specifies necessary admin rights for a protected route.
        Used for most of the "administrative" endpoints that revolve around managing the database.
        """
        def wrapper(fn):
            @wraps(fn)
            def decorator(*args, **kwargs):
                verify_jwt_in_request()
                claims = get_jwt()
                if claims["is_admin"]:
                    return fn(*args, **kwargs)
                else:
                    return jsonify(msg="This action requires administrator permissions.\n")
            return decorator
        return wrapper

    ################### Manage DB ###################

    @app.route(f'{api_path}cleanDB', methods=['PUT', 'POST'])
    #@admin_required()
    def cleanDB() -> Response:
        """
        Completely cleans the Elasticsearch database, removing all indices and records.
        """
        try:
            return createResponse(metaboserv.cleanDB())
        except Exception:
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}resetDB', methods=['PUT', 'POST'])
    #@admin_required()
    def resetDB() -> Response:
        """
        Resets the ES database, removing all documents (keeps incides and their schemas).
        """
        try:
            return createResponse(metaboserv.resetDB(admin_pw))
        except Exception:
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}createIndex', methods=['PUT', 'POST'])
    #@admin_required()
    def createIndex() -> Response:
        """
        Creates an ElasticSearch index.

        Takes two form parameters:
        :param index: The name of the index to be created
        :param schema: The "class" of the index (e.g. study or source)
        :returns 200: Success, 400: Index name missing, 409: Index already exists
        """
        try:
            index_name = request.form.get('index', None)
            index_schema = request.form.get('schema', None)
            if not index_name:
                return jsonify({'msg': "No index name was provided!"}), 400
            return createResponse(metaboserv.createIndex(index_name, index_schema))
        except Exception:
            return createErrorResponse(mandatory_parameters=["index", "schema"])

    @app.route(f'{api_path}deleteIndex', methods=['PUT', 'POST'])
    #@admin_required()
    def deleteIndex() -> Response:
        """
        Deletes an ElasticSearch index.

        Takes one form parameter:
        :param index: The name of the index to be deleted
        :returns 200: Success, 400: Index name missing, 404: Index not found
        """
        try:
            index_name = request.form.get('index', None)
            if not index_name:
                return jsonify({'msg': "No index name was provided!"}), 400
            return createResponse(metaboserv.deleteIndex(index_name))
        except Exception:
            return createErrorResponse(mandatory_parameters=["index"])

    @app.route(f'{api_path}getIndex', methods=['GET'])
    @admin_required()
    def getIndex() -> Response:
        """
        Queries the database for information about an ElasticSearch index.

        Takes one query parameter:
        :param index: The name of the index to GET
        :returns 200: Index information, 400: Index name missing, 404: Index not found
        """
        try:
            index_name = request.args.get('index', None)
            if not index_name:
                return jsonify({'msg': "No index name was provided!"}), 400
            return createResponse(metaboserv.getIndex(index_name))
        except Exception:
            return createErrorResponse(mandatory_parameters=["index"])

    @app.route(f'{api_path}createCache', methods=['PUT', 'POST'])
    @admin_required()
    def createCache() -> Response:
        """
        Creates a cache of the existing data for faster access.
        """
        try:
            return createResponse(metaboserv.createInitialCache())
        except Exception:
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}deleteRecord', methods=['PUT', 'POST'])
    @admin_required()
    def deleteRecord() -> Response:
        """
        Deletes a database record from ElasticSearch.

        Takes two form parameters:
        :param id: The name of the record to delete
        :param index: The name of the index the record lives in
        :returns 200: Success, 400: Record ID or Index name missing, 404: Record not found
        """
        try:
            record_id = request.form.get('id', None)
            index_name = request.form.get('index', None)
            if not record_id or not index_name:
                return jsonify({'msg': "No record identifier or no index name was provided!"}), 400
            return createResponse(metaboserv.deleteRecord(index_name, record_id))
        except Exception:
            return createErrorResponse(mandatory_parameters=["id", "index"])

    @app.route(f'{api_path}deleteRecordsByIndex', methods=['PUT', 'POST'])
    #@admin_required()
    def deleteRecordsByIndex() -> Response:
        """
        Deletes all ElasticSearch records in an ElasticSearch index (e.g. all sources)

        Takes one form parameter:
        :param index: The name of the index
        :returns 200: Success, 400: Index name missing, 404: Index not found
        """
        try:
            index_name = request.form.get('index', None)
            if not index_name:
                return jsonify({'msg': "No index name was provided!"}), 400
            return createResponse(metaboserv.deleteRecordsByIndex(index_name))
        except Exception:
            return createErrorResponse(mandatory_parameters=["index"])

    @app.route(f'{api_path}getRecord', methods=['GET'])
    #@admin_required()
    def getRecord() -> Response:
        """
        Retrieves an arbitrary record from the database.

        Takes two query parameters (mandatory):
        :param index: Name of the index
        :param id: ID of the document
        """
        try:
            index = request.args.get('index', None)
            record_id = request.args.get('id', None)
            if not index or not record_id:
                return jsonify("Either index or document ID is missing!")
            return createResponse(metaboserv.getRecord(index, record_id))
        except Exception as e:
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['index', 'id'])

    ################### Manage studies ###################

    @app.route(f'{api_path}getStudy', methods=['GET'])
    @jwt_required(optional=True)
    def getStudy() -> Response:
        """
        Returns data for a study (a collection of experiments), e.g. the AKI study.
        The AKI study dealt with Acute Kidney Injury Patients and collected NMR data (Bruker Avance, urine).
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes one query parameter:
        :param study: The name of the study to GET
        :returns 200: Study JSON, 400: Study ID missing, 401: Unauthorized, 404: Study not found
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            study = request.args.get('study', None)
            if not study:
                return jsonify({"No study identifier was provided!"}), 400
            if current_user:
                return createResponse(metaboserv.getStudy(study, user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getStudy(study, auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=["study"])

    @app.route(f'{api_path}getStudyList', methods=['GET'])
    @jwt_required(optional=True)
    def getStudyList() -> Response:
        """
        GETs a list of all study records that are currently contained in the ES database.
        Can use auth tokens from HTTP headers or JWT for authentication.

        :returns 200: Study list (JSON)
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            if current_user:
                return createResponse(metaboserv.getListOfStudies(user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getListOfStudies(auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}checkName', methods=['GET'])
    def checkName() -> Response:
        """
        Checks if either a username or study title already exists.

        Takes two form parameters:
        :param type: Either "user" or "study" (optional, defaults to study)
        :param id: The identifier in question
        :returns 200: Yes/No (in JSON), 400: ID missing
        """
        try:
            identifier = request.args.get('id', None)
            if not identifier:
                return jsonify({'msg': "ID is missing!"}), 400
            identifier_type = request.args.get('type', "study")
            if identifier_type == "study":
                return jsonify({'id': identifier, 'exists': metaboserv.recordExists(metaboserv.study_index, identifier)}), 200
            else:
                return jsonify({'id': identifier, 'exists': metaboserv.recordExists(metaboserv.user_index, identifier)}), 200
        except Exception as e:
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['id'])

    @app.route(f'{api_path}getSourcesInStudy', methods=['GET'])
    @jwt_required(optional=True)
    def getSourcesInStudy() -> Response:
        """
        GETs a list of all source entries that are in the provided study.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes one query parameter:
        :param study: Study to look in. Defaults to AKI
        :returns 200: Source List (JSON), 401: Unauthorized, 404: Study not found
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            study = request.args.get('study', "AKI")
            if current_user:
                return createResponse(metaboserv.getSourcesInStudy(study, user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getSourcesInStudy(study, auth_token=auth_token))
        except:
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}updateStudyMetadata', methods=['PUT', 'POST'])
    @jwt_required(optional=True)
    def updateStudyMetadata() -> Response:
        """
        Adds metadata to a study or updates existing parameters.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes two JSON parameters:
        :param study: The study in question
        :param metadata: Metadata object

        :returns 200: Success, 400: Study ID missing, 404: Study not found
        """
        try:
            logging.info(request.json)
            study = request.json.get("study", None)
            auth_token = request.headers.get("PERM-TOKEN", None)
            if not study:
                return jsonify({'msg': "No study ID was provided!"}), 400
            if not metaboserv.recordExists(metaboserv.study_index, study):
                return jsonify({'msg': f"Study {study} does not exist!"}), 404
            study_dict = request.json.get("metadata", None)
            if not study_dict:
                return jsonify({'msg': "No metadata was attached!"})
            if "other" in study_dict.keys():
                for extra_kv_pair in study_dict["other"].split("|"):
                    kv = extra_kv_pair.split(":")
                    study_dict[kv[0]] = kv[1]
                del study_dict["other"]

            if current_user:
                return createResponse(metaboserv.updateStudyMetadata(study, study_dict, user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.updateStudyMetadata(study, study_dict, auth_token=auth_token))
        except Exception as e:
            logging.error(e)
            return createErrorResponse(mandatory_parameters=["study"])

    @app.route(f'{api_path}getPermittedUsers', methods=['GET'])
    @jwt_required()
    def getPermittedUsers() -> Response:
        """
        Retrieves a list of users with permissions to view or manage a specific study.

        Has one query parameter:
        :param study: The study in question
        :returns 200: List of users with permissions for study, 400: Study ID missing, 401: Unauthorized, 404: Study not found
        """
        try:
            study = request.args.get('study', None)
            if not study:
                return jsonify({"No study identifier was provided!"}), 400
            if not current_user:
                return jsonify({"Encountered JWT Error!"}), 400
            return createResponse(metaboserv.getPermittedUsers(study, current_user))
        except Exception:
            return createErrorResponse(mandatory_parameters=["study"])

    ################### Source metadata, conc. for single sources ###################

    @app.route(f'{api_path}getSource', methods=['GET'])
    @jwt_required(optional=True)
    def getSource() -> Response:
        """
        Retrieves metadata for a source, e.g. a particular patient.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes one query parameter:
        :param source: The name of the source to GET
        :returns 200: Source JSON, 400: Source ID missing, 401: Unauthorized, 404: Source not found
        """
        try:
            source = request.args.get('source', None)
            auth_token = request.headers.get('PERM-TOKEN', None)
            if not source:
                return jsonify({'msg': "No Source ID was provided!"}), 400
            if current_user:
                return createResponse(metaboserv.getSource(source, user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getSource(source, auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=["source"])

    @app.route(f'{api_path}getConcentrationData', methods=['GET'])
    @jwt_required(optional=True)
    def getConcentrationData() -> Response:
        """
        Queries the database for concentration data of a particular source ID.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes one query parameter:
        :param source: The name of the source to GET data for
        :returns 200: Concentration data, 400: Source ID missing, 401: Unauthorized, 404: Source not found
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            source = request.args.get('source', None)
            if not source:
                return jsonify({'msg': "No source identifier was provided!"}), 400
            if current_user:
                return createResponse(metaboserv.getConcentrationData(source, user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getConcentrationData(source, auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=["source"])

    ################### Manage metabolites ###################

    @app.route(f'{api_path}getMetabolite', methods=['GET'])
    @jwt_required(optional=True)
    def getMetabolite() -> Response:
        """
        Queries the database for information about a metabolite.
        Both the "normal name" as well as the internal identifier (snake case) can be provided.
        Can use auth tokens from HTTP headers or JWT for authentication.
        Authentication only influences which studies are shown (metabolite X is recorded in studies Y,Z and A..)

        Takes one parameter:
        :param metabolite: The name of the metabolite to GET
        :returns 200: Metabolite JSON, 400: Metabolite identifier missing, 404: Not found
        """
        try:
            meta_id = request.args.get('metabolite', None)
            auth_token = request.headers.get('PERM-TOKEN', None)
            if not meta_id:
                return jsonify({'msg': "No metabolite identifier was provided!"}), 400
            if current_user:
                return createResponse(metaboserv.getMetabolite(meta_id, user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getMetabolite(meta_id, auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=["metabolite"])

    @app.route(f'{api_path}updateMetabolite', methods=['PUT', 'POST'])
    @admin_required()
    def updateMetabolite() -> Response:
        """
        Adds or changes arbitrary fields in a metabolite entry.

        Takes an ID and a string as form parameter:
        :param metabolite: Identifier of the metabolite
        :param fields: Semicolon separated key value pairs to add/change
        :returns 200: Success, 404: Study not found, 500: Server error
        """
        try:
            metabolite = request.form.get("metabolite")
            data_object = {}
            if request.form.get("fields"):
                for extra_kv_pair in request.form.get("fields").split(";"):
                    kv = extra_kv_pair.split(":")
                    data_object[kv[0].strip()] = kv[1].strip()
            return createResponse(metaboserv.updateMetabolite(metabolite, data_object))
        except Exception:
            return createErrorResponse(mandatory_parameters=["metabolite"])  
      
    @app.route(f'{api_path}getMetaboliteList', methods=['GET'])
    @jwt_required(optional=True)
    def getMetaboliteList() -> Response:
        """
        GETs a list of all metabolites that are stored in at least one entry of the ES database.
        Uses a special "metabolites" index that only serves this one purpose.
        Can use auth tokens from HTTP headers or JWT for authentication.

        :returns 200: Metabolite list (JSON)
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            if current_user:
                return createResponse(metaboserv.getListOfMetabolites(user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getListOfMetabolites(auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}getMetaboliteListForStudy', methods=['GET'])
    @jwt_required(optional=True)
    def getMetaboliteListForStudy() -> Response:
        """
        GETs a list of all metabolites that are stored in at least one entry of the ES database.
        Only returns metabolites that are recorded in a specific study.
        Also returns coverage values for each metabolite.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Requires one query parameter:
        :param study: Study identifier to get metabolite list for
        :returns 200: Metabolite list
        """
        try:
            study = request.args.get('study', None)
            if not study:
                return jsonify("Study parameter is missing!"), 400
            auth_token = request.headers.get('PERM-TOKEN', None)
            if current_user:
                return createResponse(metaboserv.getListOfMetabolitesForStudy(study, user=current_user, auth_token=auth_token))
            else:
                return createResponse(metaboserv.getListOfMetabolitesForStudy(study, auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=["study"])

    @app.route(f'{api_path}getNormalConcentration', methods=['GET'])
    def getNormalConcentration() -> Response:
        """
        Returns applicable normal concentration ranges for a subset of metabolites, age groups, sex and units.

        Takes at least one, and up to four query parameters:
        :param metabolite (mandatory): Semicolon-separated list of metabolites to get normal concentration ranges for
        :param specimen (optional): Semicolon-separated list of biospecimen to get concentrations for
        :param agegroup (optional): Semicolon-separeted list of agegroups. Valid entries: "infant", "newborn", "children", "adolescents" or "adults".
        :param sex (optional): Can be either "male", "female" or "both". Omitting it matches all DB entries
        :param unit (optional): Semicolon-separated list of units. Omitting it matches all DB entries
        :param source (optional): Semicolon-separated list of NC sources (like HMDB) to consider. If none is provided, all are considered
        :returns Normal concentration ranges
        """
        try:
            metabolites = request.args.get('metabolite', None)
            if not metabolites:
                return jsonify("No metabolite was provided!"), 400
            metabolite_list = metabolites.split(";")
            age = request.args.get('agegroup', None)
            sex = request.args.get('sex', 'not specified')
            unit = request.args.get('unit', None)
            specimen = request.args.get('specimen', None)
            source = request.args.get('source', None)
            if unit: unit = unit.split(";")
            if specimen: specimen = specimen.split(";")
            if age: age = age.split(";")
            if source: source = source.lower().split(";")
            return createResponse(metaboserv.getNormalConcentration(metabolite_list, specimen, age, sex, unit, source))
        except Exception:
            return createErrorResponse(mandatory_parameters=["metabolite"])

    ################### Phenotype data ###################

    @app.route(f'{api_path}getPhenotypeData', methods=['GET'])
    @jwt_required(optional=True)
    def getPhenotypeData() -> Response:
        """
        Retrieves phenotype data from the database.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes at least one form parameter:
        :param study: Studies to consider (semicolon-separated)
        :param phenotype: Phenotypes to consider (semicolon-separated)
        :returns 200 Phenotype data, 400 Bad request, 401 Unauthorized
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            study_string = request.args.get('study', None)
            phenot_string = request.args.get('phenotype', None)
            if not study_string and not phenot_string:
                return jsonify({'msg': "You must provide at least one search parameter!"}), 400
            studies = study_string.split(";") if study_string else None
            phenotypes = phenot_string.lower().replace(' ', '_').split(";") if phenot_string else None
            return createResponse(metaboserv.getPhenotypeData(phenotypes=phenotypes, studies=studies, user=current_user, auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}getPhenotypeMetadata', methods=['GET'])
    @jwt_required(optional=True)
    def getPhenotypeMetadata() -> Response:
        """
        Retrieves phenotype metadata from the database,
        i.e. data about how a particular phenotype was determined or how data was gathered.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes at least one form parameter:
        :param study: Study to consider
        :param source: Sources/patients to consider
        :returns 200 Phenotype metadata, 400 Bad request, 401 Unauthorized
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            study = request.args.get('study', None)
            sources_string = request.args.get('source', None)
            if not study or not sources_string:
                return jsonify({'msg': "Query parameters are missing!"}), 400
            source_ids = sources_string.split(";")
            return createResponse(metaboserv.getPhenotypeMetadata(study=study, source_ids=source_ids, user=current_user, auth_token=auth_token))
        except Exception:
            return createErrorResponse(mandatory_parameters=['study', 'source'])

    @app.route(f'{api_path}getPhenotypeList', methods=['GET'])
    @jwt_required(optional=True)
    def getPhenotypeList() -> Response:
        """
        Retrieves a list of phenotypes and their according metadata.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes one optional query parameter:
        :param study: The studies to consider, semicolon-separated (considers all if omitted)
        :returns 200 List of phenotypes, 401 Unauthorized
        """
        try:
            auth_token = request.headers.get('PERM-TOKEN', None)
            study_string = request.args.get('study', None)
            if study_string: studies = study_string.split(";")
            else: studies = None
            return createResponse(metaboserv.getPhenotypeList(studies=studies, user=current_user, auth_token=auth_token))
        except:
            return createErrorResponse(mandatory_parameters=None)

    ################### Database queries ###################

    @app.route(f'{api_path}filterByMetabolites', methods=['GET'])
    @jwt_required(optional=True)
    def filterByMetabolites() -> Response:
        """
        Returns a subset of all contained data, filtered by the study and metabolites of interest.
        Requires a semicolon-separated list of metabolite queries.
        Metabolite queries are separated with pipe symbols (|).
        Out of bounds queries (oob) check if a value is *outside* of the provided values.
        Example: 'valine;leucine|0.45;2-hydroxybutyrate|0.01-0.07;asparagine|oob|0.03-0.17'
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes two mandatory form parameters:
        :param study: Semicolon-separated list of studies to consider
        :param metabolites: Semicolon-separated list of metabolite queries as described above.
        Further takes three form parameters for plotting:
        :param plot_id: Identifier to use for any created plot
        :param plot_command: Specifies which plots to create.
        :param plot_nconc: Normal concentration values to use for the plot
        And some more optional parameters:
        :param mode: Full or partial (see database function)
        :param phenotype: List of phenotype data to consider (for table and plots)
        :returns 200: Data subset, 400: Parameters missing
        """
        try:
            # Read query parameters (missing: strictness, ctu etc.)
            metabolites = request.args.get('metabolite', None)
            if not metabolites: return jsonify("You must provide at least one metabolite!"), 400
            study = request.args.get('study', None)
            if not study: return jsonify("You must provide at least one study!"), 400
            plot_id = request.args.get('plot_id', None)
            generate_plots = True if plot_id else False
            plot_command = request.args.get('plot_command', 'hist')
            plot_nconc = request.args.get('plot_nconc', "")
            mode = request.args.get('mode') if (request.args.get('mode') and request.args.get('mode') in ['full', 'partial']) else 'partial'
            phenotype_string = request.args.get('phenotype', None)
            phenotypes = phenotype_string.split(";") if phenotype_string else None
            auth_token = request.headers.get('PERM-TOKEN', None)

            metabolite_ranges = {}

            # Split metabolites and studies
            metabolites_split = metabolites.split(";")
            studies = study.split(";")

            # Isolate metabolite ranges
            for metabolite_query in metabolites_split:

                # Save normalized metabolite name
                metabolite_query_split = metabolite_query.split("|")
                metabolite_normalized = metabolite_query_split[0].lower().replace(" ", "_")
                if metabolite_normalized in KNOWN_SYNONYMS.keys():
                    metabolite_normalized = KNOWN_SYNONYMS[metabolite_normalized]

                # Case 1: Any metabolite value
                if len(metabolite_query_split) == 1:
                    metabolite_ranges[metabolite_normalized]= {'query_type': 'any'}
                # Case 2: Min (and maybe max)
                elif len(metabolite_query_split) == 2:
                    ranges = metabolite_query_split[1].split("-")
                    if len(ranges) == 1: metabolite_ranges[metabolite_normalized] = {'query_type': 'min', 'min': float(ranges[0])}
                    else: metabolite_ranges[metabolite_normalized] = {'query_type': 'minmax', 'min': float(ranges[0]), 'max': float(ranges[1])}
                # Case 3: OOB Query (out of normal bounds)
                else:
                    ranges = metabolite_query_split[2].split("-")
                    metabolite_ranges[metabolite_normalized] = {'query_type': 'oob', 'min': float(ranges[0]), 'max': float(ranges[1])}

            logging.info("Query with phenotypes:")
            logging.info(phenotypes)
            if current_user:
                query_results = metaboserv.filterByMetabolites(studies, metabolite_ranges, mode, user=current_user, auth_token=auth_token, phenotypes=phenotypes).getBody()
            else:
                query_results = metaboserv.filterByMetabolites(studies, metabolite_ranges, mode, auth_token=auth_token, phenotypes=phenotypes).getBody()

            #logging.info(query_results)
            go_forth = False
            if generate_plots:
                go_forth = False
                for study in query_results.keys():
                    if query_results[study]:
                        go_forth=True
                        break

            if go_forth:
                plot_list = plot_command.split(";")
                metaboserv.generatePlots(plot_list=plot_list, plot_id=plot_id, data_to_plot=copy.deepcopy(query_results), plot_nconc=plot_nconc, phenotypes=phenotypes)
            return createResponse(ResultObject(body=query_results))
        except Exception as e:
            logging.error("Failed querying concentration data.")
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['metabolites', 'study'])

    @app.route(f'{api_path}getRawSpectrum_Bruker', methods=['GET'])
    @jwt_required(optional=True)
    def getRawSpectrum_Bruker() -> Response:
        """
        Gets two vectors (x,y axes) of a spectrum in Bruker format.
        x axis is ppm-based, y depicts the magnitude of the spectrum signal.
        Can use auth tokens from HTTP headers or JWT for authentication.
        
        Takes query parameters:
        :param id: The ID of the source to get the raw spectrum for
        :param study: The name of the study (used for permission checking)
        :returns 200: Two vectors (JSON), 400: ID or study name missing, 401: Unauthorized
        """
        try:
            source = request.args.get('source', None)
            study = request.args.get('study', None)
            auth_token = request.headers.get('PERM-TOKEN', None)
            if not study or not source:
                return jsonify({'msg': 'Study or source ID missing!'}), 400
            if not metaboserv.checkUserPermission(current_user if current_user else None, study) and not metaboserv.checkTokenPermission(auth_token, study):
                return jsonify({'msg': "You don't have the required permission to access this data!"}), 401
            pattern = metaboserv.getStudyPattern(study)
            return createResponse(metaboserv.parseRawSpectrum_Bruker(source, pattern))
        except:
            return createErrorResponse(mandatory_parameters=["source", "study"])
    
    ################### Token Auth ##################

    @app.route(f'{api_path}generateToken', methods=['PUT', 'POST'])
    @jwt_required()
    def generateAuthToken() -> Response:
        """
        Generates a new token which allows the holder to view or manage data.

        Takes some form parameters:
        :param study: Studies the token is for, semicolon separated
        :param expiration: Expiration date
        :param perm_type: Permission type (view/manage)
        :returns 200 Token string, 400: Study missing
        """
        try:
            if not current_user:
                return jsonify({'msg':'Login required!'}), 401
            study = request.form.get('study', None)
            if not study: return jsonify({'msg': 'Study parameter is missing!'}), 400
            studies_for_token = study.strip().split(";")
            studies_for_token = list(filter(lambda x: metaboserv.checkUserPermission(current_user, x, "manage"), studies_for_token))
            if not studies_for_token:
                return jsonify({'msg': 'You do not have the permission to generate tokens for any provided study!'}), 401
            try:
                expiration = int(request.form.get('expiration', -1))
            except ValueError: expiration = -1
            comment = request.form.get('comment', None)
            perm_type = request.form.get('perm_type', 'view')
            return createResponse(metaboserv.generateToken(studies_for_token, expiration, perm_type, comment, user=current_user))
        except Exception as e:
            logging.error(e)
            return createErrorResponse(mandatory_parameters=["study"])

    @app.route(f'{api_path}revokeToken', methods=['PUT', 'POST'])
    @jwt_required()
    def revokeToken() -> Response:
        """
        Revokes a token.

        Takes one form parameter:
        :param id: ID for the token
        :returns 200: Success, 400: Token ID missing, 401: Unauthorized, 404: Token not found
        """
        try:
            if not current_user: return jsonify("Login required!"), 401
            token_id = request.form.get('id', None)
            if not token_id: return jsonify("You must provide a token string!"), 400
            return createResponse(metaboserv.revokeToken(token_id, current_user))
        except:
            return createErrorResponse(mandatory_parameters=["id"])

    @app.route(f'{api_path}getTokenList', methods=['GET'])
    @jwt_required()
    def getTokenList() -> Response:
        """
        Retrieves a list of all tokens issued by a user.

        Takes up to two form parameters:
        :param study: Study to get tokens for
        :param username: Name of the user to get tokens for (defaults to issuing user)
        :returns 200: Token list, 401: Unauthorized
        """
        try:
            if not current_user: return jsonify("Login required!"), 401
            logging.info(current_user.username)
            username = request.args.get('username', current_user.username)
            study = request.args.get('study', None)
            resp = metaboserv.getTokenList(username, study, current_user)
            return createResponse(resp)
        except Exception as e:
            logging.error(e)
            return createErrorResponse(mandatory_parameters=None)

    @app.route(f'{api_path}getTokenData', methods=['GET'])
    @admin_required()
    def getTokenData() -> Response:
        """
        Retrieves token data (author, permission type) by ID.

        Takes one query parameter:
        :param id: ID for the token
        :returns 200: Token data, 404: Token not found
        """
        try:
            token_id = request.args.get('id', None)
            if not token_id: return jsonify({'msg': f'Token {token_id} was not found!'}), 404
            return createResponse(metaboserv.getTokenData(token_id))
        except:
            return createErrorResponse(mandatory_parameters=['id'])

    @app.route(f'{api_path}testToken', methods=['GET'])
    def testToken() -> Response:
        """
        Testing method for auth tokens (not JWT).
        The auth token has to be passed in the header as that is the standard way the frontend will provide it.

        A study to check the token for also has to be provided as query parameter 'study'.
        :returns Permission check response (failed/passed)
        """
        try:
            token = request.headers.get('PERM-TOKEN', None)
            study = request.args.get('study', None)
            if not token or not study: return jsonify({'msg': 'No auth token or no study detected!'})
            if not metaboserv.checkTokenPermission(token_id=token, study=study):
                return jsonify({'msg': f"Permission check failed on study {study}."}), 401
            else:
                return jsonify({'msg': f"Permission check passed on study {study}!"}), 200
        except:
            return createErrorResponse(mandatory_parameters=['token'])


    ################### Manage files and parsing ##################

    @app.route(f'{api_path}uploadData', methods=['POST'])
    @jwt_required()
    def uploadData() -> Response:
        """
        Uploads study files, so they can be validated and used in processing later.
        Allowed extensions and maximum size can be set in config.

        Expects at least a concentration data file and a study identifier.
        Metadata file and phenotype data file can optionally be added, as well.
        File parameter names are 'datafile', 'mdfile' and 'phenofile'.

        :returns Status message about success or failure of the upload and validation procedure
        """
        try:
            if not os.path.exists (app.config["UPLOAD_FOLDER"]):
                os.mkdir(app.config["UPLOAD_FOLDER"])
            study = request.form.get("study", None)
            if not study:
                return jsonify({'msg': "Study identifier is missing!"}), 400
            if not current_user:
                return jsonify({'msg': "User is not authorized for upload procedure!"}), 401
            if metaboserv.recordExists(study):
                return jsonify({'msg': "Sorry, this study name is already taken!"}), 409
            mdfile = request.files.get('metadata_file', None)
            phenofile = request.files.get('pheno_file', None)
            datafile = request.files.get('data_file', None)
            if not datafile:
                return jsonify({'msg': "Concentration data file is missing!"}), 400
            uploaded_files = [datafile, mdfile, phenofile]
            file_upload_response = handleFileUpload(study, uploaded_files, app.config["UPLOAD_FOLDER"], app.config["UPLOAD_EXTENSIONS"])
            if file_upload_response.status == 200:
                res_dict = file_upload_response.getBody()
                metaboserv.addToPendingFileList(study, res_dict['filepaths'], res_dict['extensions'])
                return createResponse(file_upload_response)
            else:
                handleFileDeletion(os.path.join(app.config["UPLOAD_FOLDER"], study, datafile.filename))
                if mdfile: handleFileDeletion(os.path.join(app.config["UPLOAD_FOLDER"], study, mdfile.filename))
                if phenofile: handleFileDeletion(os.path.join(app.config["UPLOAD_FOLDER"], study, phenofile.filename))
                return createResponse(file_upload_response)
        except Exception as e:
            logging.error(e)
            return Response(f"Failed uploading the provided file. Make sure the file is of .csv, .xlsx or .pdf type and not larger than 50MB.", 500)

    @app.route(f'{api_path}deleteFile')
    @admin_required()
    def deleteFile() -> Response:
        """
        Deletes a file from the upload folder.
        (This will rarely be necessary, just for completeness' sake)

        Takes one form parameter:
        :param file: Name of the file
        :param study: Name of the according study (only considered when provided)
        :returns 200: Success, 400: Filename missing, 404: File not found
        """
        try:
            filename = request.form.get('file')
            if not filename:
                return jsonify({'msg': "Filename was not provided!"}), 400
            study = request.form.get('study', None)
            if not study:
                abs_filename = os.path.join(app.config['UPLOADS_FOLDER'], filename)
            else:
                abs_filename = os.path.join(app.config['UPLOADS_FOLDER'], study, filename)
            return createResponse(handleFileDeletion(abs_filename))
        except:
            return createErrorResponse(mandatory_parameters=["file"])

    @app.route(f'{api_path}processConcentrationFile', methods=['PUT', 'POST'])
    @jwt_required()
    def processConcentrationFile() -> Response:
        """
        Processes a  concentration file in the uploads folder in order to add/update ES objects.
        Adds records in ES if they dont exist, otherwise they are simply updated.

        Takes form parameters:
        :param study: Study ID the file belongs to
        :param header: Marks the header row index of the uploaded file (for pandas)
        :param date: Date or timeframe associated with the file
        :param type: Study type of the file (serum, urine..)
        :param file: Filename (without upload path)
        :param filetype: Type of file (xlsx, csv, tsv)
        :param unit: (Optional) unit the metabolites are measured in
        :param vis: Public or private
        :param delimiter: The delimiter used (tab, semicolon)
        :param transpose: Tells the file parsing method whether to transpose the file
        :returns 200: Success, 400: Missing parameters, 404: File not found
        """
        try:
            study = request.form.get('study', None)
            filename = request.form.get('file', None)
            if not study or not filename:
                return jsonify({'msg': "Filename or associated study was not provided!"}), 400
            filetype = request.form.get('filetype', None)
            if not filetype:
                if ".xlsx" in filename:
                    filetype = "xlsx"
                elif ".csv" in filename:
                    filetype = "csv"
                elif ".tsv" in filename:
                    filetype = "tsv"
                else:
                    return jsonify({'msg': "Filetype missing!"}), 400
            headers = int(request.form.get('header', '0'))
            date = request.form.get('date', "Unknown")
            study_type = request.form.get('type', "Unknown")
            authors = request.form.get('authors', "Unknown")
            id_prefix = request.form.get('id_prefix', None)
            vis = request.form.get('vis', "public")
            unit = request.form.get('unit', "mmol/L")
            delimiter = request.form.get('delimiter', "tab")
            transpose = request.form.get('transpose', False)
            logging.info("Handing over to database")
            return createResponse(metaboserv.addConcentrationData(study=study, filename=os.path.join(app.config["UPLOAD_FOLDER"], study, filename), headers=headers, vis=vis,
                filetype=filetype, id_prefix=id_prefix, unit=unit, date=date, st_type=study_type, authors=authors, user=current_user if current_user else None, delimiter=delimiter,
                transpose=transpose))
        except Exception:
            return createErrorResponse(mandatory_parameters=["study", "file"]) 

    @app.route(f'{api_path}processMZMLFile', methods=['PUT', 'POST'])
    @jwt_required()
    def processMZMLFile() -> Response:
        """
        Processes a .mzML (mass spectrometry) file.
        Adds records in ES if they dont exist, otherwise they are simply updated.
        """
        try:
            pass
        except Exception:
            return createErrorResponse(mandatory_parameters=["study", "file"]) 

    @app.route(f'{api_path}processPhenotypeFile', methods=['PUT', 'POST'])
    @jwt_required()
    def processPhenotypeFile() -> Response:
        """
        Processes a .csv file containing (nominal) phenotype data
        Adds records in ES if they dont exist, otherwise they are simply updated.

        Takes form parameters:
        :param study: Study ID the file belongs to
        :param header: Marks the header row index of the uploaded file (for pandas)
        :param delimiter: Delimiter for csv file (optional, defaults to 'tab'). Other valid ones are comma, semicolon
        :param id_prefix: Prefix for 'id' column in the file
        :param id_pattern: String in the format '[separator];[n],[m],..' where n, m etc. are the part of the ID string to keep
            e.g. to transform 'AKI_8_24_[id]_11029' into 'AKI_[id]': id_pattern = '_;0,3' 
        :param file: Name of the file to parse
        :param phenotype: Phenotype identifier
        :returns 200: Success, 400: Missing parameters, 404: File not found
        """
        try:
            study = request.form.get('study', None)
            filename = request.form.get('file', None)
            if not study or not filename:
                return jsonify({'msg': "Filename or associated study was not provided!"}), 400
            phenotype = request.form.get('phenotype', None)
            if not phenotype:
                return jsonify({'msg': "No phenotype identifier was provided!"}), 400

            delimiter = request.form.get('delimiter', 'tab')
            headers = int(request.form.get('header', '0'))
            id_prefix = request.form.get('id_prefix', None)
            id_pattern = request.form.get('id_pattern', None)

            logging.info("thonk")
            return createResponse(metaboserv.addNominalPhenotypeData(study=study, filename=os.path.join(app.config["UPLOAD_FOLDER"], study, filename), headers=headers,
                    id_prefix=id_prefix, id_pattern=id_pattern, delimiter=delimiter, phenotype=phenotype, user=current_user))
        except Exception as e:
            logging.error(e)
            return createErrorResponse(mandatory_parameters=["study", "file", "phenotype"]) 

    @app.route(f'{api_path}addMetabolitesFromBiocratesFile', methods=['PUT', 'POST'])
    #@admin_required()
    def addMetabolitesFromBiocratesFile() -> Response:
        """
        Parses a .csv file containing Biocrates metabolite information and MetaboINDICATOR metrics.
        For formatting please refer to the example file biocrates_q500.csv.

        Takes one form parameter:
        :param file: The filename of the file to parse
        :returns 200: Success, 400: Filename missing, 404: File not found
        """
        try:
            filename = request.form.get('file', None)
            if not filename: return jsonify("No filename was provided!"), 400
            return createResponse(metaboserv.addMetabolitesFromBiocratesFile(filename=os.path.join(app.config["UPLOAD_FOLDER"], filename)))
        except Exception as e:
            logging.error("Failed parsing biocrates data.")
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['file'])

    @app.route(f'{api_path}extractNormalConcs', methods=['PUT', 'POST'])
    #@admin_required()
    def extractNormalConcs() -> Response:
        """
        Extract normal concentrations from a file and store them in the ES database.
        A formatting hint needs to be passed to this endpoint, e.g. hmdb.

        Takes up to three form parameters:
        :param file - The file to parse (presumed to be in the nc_folder set through the config. file)
        :param format - Format of the normal concentration file. Currently available: hmdb
        :param specimen - Biospecimen to consider
        :returns 200: Success, 400: File or format missing or unknown format, 404: File not found
        """
        try:
            accepted_formats = ['hmdb']
            file = request.form.get('file', None)
            format = request.form.get('format', None)
            specimen = request.form.get('specimen', None)
            if not file: return jsonify("No filename was provided!"), 400
            if not format: return jsonify("No format indicator was provided!"), 400
            if format not in accepted_formats: return jsonify(f"Format {format} is unknown!"), 400
            if specimen: specimen = specimen.split(";")
            return createResponse(metaboserv.extractNormalConcs(file,format,specimen))
        except Exception as e:
            logging.error(f"Failed extracting normal concentrations ({file}, {format}).")
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['file', 'format'])

    ################### Plot generation and retrieval ###################

    @app.route(f'{api_path}getPlot', methods=['GET'])
    def retrievePlot() -> Response:
        """
        Retrieves a previously generated plot using the ID of the plot.

        Takes one path param:
        :param plot_id: The ID of the plot to retrieve
        :returns 200: BytesIO for plot, 400: Plot ID missing, 404: Plot with provided ID not found
        """
        try:
            plot_id = request.args.get("plot_id", None)
            if not plot_id:
                return jsonify({'msg': "No plot ID was provided!"}), 400
            plot_obj=metaboserv.generated_plots.get(plot_id)
            if not plot_obj:
                return jsonify({'msg':f"No plot object was found with the given parameter '{plot_id}'"})
            else:
                return Response(plot_obj.getvalue(), mimetype='image/png', status=200)
        except:
            return createErrorResponse(mandatory_parameters=["plot_id"])

    @app.route(f'{api_path}createScatterPlot', methods=['POST', 'PUT'])
    def createScatterPlot() -> Response:
        """
        Creates a scatterplot "on demand". An ID for the plot must therefore be supplied.

        Takes form parameters:
        :param plot_id: ID for the generated plot (for retrieval)
        :param data: Data to plot (JSON, handled by the frontend)
        :param plot_nconc: Normal concentrations
        :param phenotype: To mark phenotype keys as opposed to metabolite keys
        :return 200: Plot as a binary object, 400: Missing parameters
        """
        try:
            data_to_plot = request.form.get("data", None)
            plot_id = request.form.get("plot_id", None)
            plot_nconc = request.form.get("plot_nconc", None)
            phenotype_string = request.form.get("phenotype", None)
            phenotypes = phenotype_string.split(";") if phenotype_string else None
            if not data_to_plot or not plot_id:
                return jsonify({'msg': "Plot ID or data to plot was not provided!"}), 400
            else:
                return createResponse(metaboserv.generatePlots("scatter",
                 plot_id, json.loads(data_to_plot), plot_nconc, phenotypes=phenotypes))
        except Exception:
            return createErrorResponse(mandatory_parameters=['plot_id', 'data'])

    @app.route(f'{api_path}createHistPlot', methods=['POST', 'PUT'])
    def createHistPlot() -> Response:
        """
        Creates a histogram "on demand". An ID for the plot must therefore be supplied.

        Takes two form parameters:
        :param plot_id: ID for the generated plot (for retrieval)
        :param data: Data to plot (JSON, handled by the frontend)
        :param plot_nconc: Normal concentrations
        :param plot_bins: Number of bins for histogram
        :param phenotype: To mark phenotype keys as opposed to metabolite keys
        :param histstyle: Histogram style
        :return 200: Plot as a binary object, 400: Missing parameters
        """
        try:
            data_to_plot = request.form.get("data", None)
            plot_id = request.form.get("plot_id", None)
            plot_nconc = request.form.get("plot_nconc", None)
            phenotype_string = request.form.get("phenotype", None)
            phenotypes = phenotype_string.split(";") if phenotype_string else None
            histstyle_string = request.form.get("histstyle", "step|fill")
            if not data_to_plot or not plot_id:
                return jsonify({'msg': "Plot ID or data to plot was not provided!"}), 400
            if "plot_bins" in request.form:
                plot_bins = request.form.get("plot_bins")
            else:
                plot_bins = None
            return createResponse(metaboserv.generatePlots("hist", plot_id, json.loads(data_to_plot), plot_nconc, plot_bins,
             phenotypes=phenotypes, histstyle=histstyle_string))
        except Exception:
            return createErrorResponse(mandatory_parameters=['plot_id', 'data'])

    @app.route(f'{api_path}createCorrelationPlot', methods=['POST', 'PUT'])
    def createCorrelationPlot() -> Response:
        """
        Creates a correlation plot/heatmap "on demand". An ID for the plot must therefore be supplied.

        Takes two form parameters:
        :param plot_id: ID for the generated plot (for retrieval)
        :param data: Data to plot (JSON, handled by the frontend)
        :return 200: Plot as a binary object, 400: Missing parameters
        """
        try:
            data_to_plot = request.form.get("data", None)
            plot_id = request.form.get("plot_id", None)
            if not data_to_plot or not plot_id:
                return jsonify({'msg': "Plot ID or data to plot was not provided!"}), 400
            else:
                return createResponse(metaboserv.generatePlots("corrmap", plot_id, json.loads(data_to_plot)))
        except Exception:
            return createErrorResponse(mandatory_parameters=['plot_id', 'data'])

    ################### Source File generation and retrieval ##################

    @app.route(f'{api_path}getSourceFile', methods=['GET'])
    @jwt_required(optional=True)
    def getSourceFile() -> Response:
        """
        Retrieves a source file from the designated source folder.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes one query parameter:
        :param file: The filename of the file to get (case sensitive)
        :param study: The study to return the source file for (used to navigate the folder, defaults to AKI)
        :returns 200: File, 400: Filename missing, 401: Unauthorized, 404: File with name not found
        """
        try:
            filename = request.args.get("file", None)
            study = request.args.get("study", "AKI")
            auth_token = request.headers.get('PERM-TOKEN', None)
            if not filename:
                return jsonify({'msg': "No filename supplied!"}), 400
            if not metaboserv.checkUserPermission(current_user if current_user else None, study) and not metaboserv.checkTokenPermission(auth_token, study):
                return jsonify({'msg': "You don't have the necessary permissions to download this file."}), 401
            if not os.path.isfile(sourcefile_path+"/" + study + "/" + filename):
                return jsonify({'msg': "No file with the provided name found."}), 404
            return send_from_directory(sourcefile_path+"/"+study, filename, as_attachment=True)
        except:
            return createErrorResponse(mandatory_parameters=['file'])

    @app.route(f'{api_path}getSourceArchive', methods=['GET'])
    @jwt_required(optional=True)
    def getSourceArchive() -> Response:
        """
        Retrieves several source files at once from the designated source folder.
        For convenience, they are packed into a .tar archive.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Takes up to two query parameters:
        :param file: Semicolon-separated string containing the file names (case sensitive)
        :param study: Name of the study in question (defaults to AKI)
        :returns 200: Archive, 400: <2 files provided, 401: Unauthorized, 404: No files found
        """
        try:
            filenames = request.args.get("file").split(";") if request.args.get("file") else None
            study = request.args.get("study", "AKI")
            auth_token = request.args.get('PERM-TOKEN', None)
            archive_name = study + "_source.tar.gz"

            if not filenames or len(filenames) == 1:
                return jsonify({'msg': "None or too few files supplied!"}), 400
            if not metaboserv.checkUserPermission(current_user if current_user else None, study) and not metaboserv.checkTokenPermission(auth_token, study):
                return jsonify({'msg': "You don't have the necessary permissions to download this archive."}), 401
            tarchive = tarfile.open(os.getcwd()+"/"+archive_name, "w:gz")
            for filename in filenames:
                full_filename = sourcefile_path + "/" + study + "/" + filename
                if (os.path.exists(full_filename) and os.path.isfile(full_filename)):
                    tarchive.add(full_filename, arcname=os.path.basename(full_filename))
            if not tarchive.getmembers():
                return jsonify({'msg': "No matching files found. Please check the filenames."}), 404
            tarchive.close()
            return send_from_directory(os.getcwd(), archive_name, as_attachment=True)
        except Exception:
            return createErrorResponse(mandatory_parameters=['file'])
        finally:
            os.remove(os.getcwd()+"/"+archive_name)

    @app.route(f'{api_path}generateCSV', methods=['PUT', 'POST'])
    @jwt_required(optional=True)
    def generateCSV() -> Response:
        """
        Generates a .csv file for a given subset of data.
        Accepts data in the ES output JSON format.
        The .csv file can then be queried by calling the retrieveCSV endpoint.
        Can use auth tokens from HTTP headers or JWT for authentication.

        Form parameters:
        :param filename: The desired filename
        :param data: The data to load into a dict and turn into CSV
        :param metabolites: The query metabolites to put in first column of the CSV file
        :param na: How to handle NAs (blank, or replace it with a symbol)
        :param sep: The separator to use for the csv file
        :returns 200: Successfully created, 400: Filename or metabolites missing, 401: Unauthorized
        """
        try:
            filename = request.form.get('filename', None)
            metabolites = request.form.get('metabolites').split(";") if request.form.get('metabolites') else None
            na = request.form.get('na', "blank")
            data = json.loads(request.form.get('data'))
            sep = request.form.get('sep', '\t')
            if not filename:
                return jsonify({'msg': "No filename supplied!"}), 400
            if not metabolites:
                return jsonify({'msg': "No metabolite list provided!"}), 400
            return createResponse(metaboserv.generateCSV(data_to_write=data, metabolites=metabolites, separator=sep, filename=filename, na=na,
            user=current_user if current_user else None))
        except Exception:
            return createErrorResponse(mandatory_parameters=['data', 'filename'])

    @app.route(f'{api_path}retrieveCSV', methods=['GET'])
    def retrieveCSV() -> Response:
        """
        Retrieves and sends the last generated .csv file, then deletes it.

        :param id: ID the CSV file was generated with
        :returns 200: CSV file, 400: CSV file ID missing, 404: File not found
        """
        try:
            csv_id = request.args.get('id', None)
            if not csv_id:
                return jsonify({'msg': 'No ID was provided to fetch the CSV file with.'}), 400
            if not metaboserv.generated_csv[csv_id]:
                return jsonify({'msg': "No generated CSV file was found."}), 404
            return send_from_directory(os.getcwd(), metaboserv.generated_csv[csv_id], as_attachment=True)
        except Exception:
            return createErrorResponse(mandatory_parameters=None)
        finally:
            if csv_id:
                os.remove(os.getcwd()+"/"+metaboserv.generated_csv[csv_id])
                metaboserv.generated_csv[csv_id] = None

    ################### User Management and Login ##################

    @jwt.user_lookup_loader
    def userLookUp(_jwt_header, jwt_data):
        """
        Callback function that looks up a user in ES database whenever a protected route is queried.

        :param _jwt_header: Header of the JWT
        :param jwt_data: Main data of the JWT
        :returns An object representing the user
        """
        id = jwt_data["sub"]
        return dict2obj(metaboserv.getUserInternal(id).getBody())

    @jwt.user_identity_loader
    def identityLookup(user) -> str:
        """
        Callback function that returns the identity object in a JSON serializable format.

        :param user: A user object
        :returns The username pertaining to the user object
        """
        return user.username

    @jwt.unauthorized_loader
    def unauthorizedCallback(callback_response: str) -> Response:
        """
        Redirects to the login page if unauthorized access occurs.

        :param callback_response: A response to return (currently unused)
        :returns Response given for unauthorized access
        """
        return jsonify({'msg': "You lack the required permissions to perform this action."}), 401

    @app.route(f'{api_path}register', methods=['POST'])
    def register() -> Response:
        """
        Open endpoint that allows the creation of user accounts.
        Different from the createUser admin method that is not meant for frontend use.

        Parameters must be given in JSON format:
        :param username: The username for the new account
        :param password: The password for the new account (will be hashed and salted)
        :param email: The E-Mail Address of the user
        :param inst: The institution of the user (optional, defaults to "Unknown" in frontend)
        :returns 200: Success, 400: Parameter missing, 401: Unauthorized username (public/admin), 409: Already exists
        """
        try:
            username = request.json.get("username", None)
            password = request.json.get("password", None)
            email = request.json.get("email", None)
            inst = request.json.get("institution", "Unknown")

            if not username or not password or not email:
                return jsonify({'msg': "At least one mandatory parameter is missing!"}), 400
            if username.lower() in ['public', 'admin', 'any']:
                return jsonify({'msg': "Provided username is protected and can not be used."}), 401
            if metaboserv.recordExists('users', username.lower()):
                return jsonify({'msg': "A user with that name exists already."}), 409
            return createResponse(metaboserv.createUser(username, password, email, inst))
        except Exception as e:
            logging.error(f"Account registration failed for {username}.")
            logging.error(e)
            return jsonify(f"Account registration failed for {username} because of a server error."), 500

    @app.route(f'{api_path}login', methods=['POST'])
    def login() -> Response:
        """
        Login function that expects a username and password in JSON data.
        If permission checking was successful, it creates and returns a JWT token.

        :param username: The username in question
        :param password: The password in question
        :param cookie: If true, also sets an access cookie. Does not seem to currently work.
        :returns 200: Access Token (JWT), 400: Username or passowrd missing, 401: Login failed
        :remarks Does not throw a 404 if a user does not exist to add some additional privacy
        """
        try:
            username = request.json.get("username", None)
            password = request.json.get("password", None)
            cookie = True if request.json.get("cookie") and request.json.get("cookie") == "true" else False
            additional_claims = None

            if not username or not password:
                return jsonify({'msg': "Username or password missing!"}), 400
            user = dict2obj(metaboserv.checkUser(username, password)) #Returns none if user not found or password fails
            if not user:
                return jsonify({'msg': "Wrong username or password!"}), 401

            additional_claims = {'view': user.permissions.view, 'manage': user.permissions.manage}
            if user.username == "admin" and user.role == "admin":
                additional_claims["is_admin"] = True
            else:
                additional_claims["is_admin"] = False

            access_token = create_access_token(identity = user, additional_claims=additional_claims)
            if cookie:
                response = jsonify({
                    "msg": "Login successful!",
                    "access_token": access_token,
                    "csrf_token": get_csrf_token(access_token)
                })
                set_access_cookies(response, access_token) #This does nothing I suppose?

            return jsonify(access_token = access_token) if not cookie else response
        except Exception as e:
            logging.error(f"Login failed for {username}.")
            logging.error(e)
            return jsonify(f"Login failed for {username} because of a server error."), 500

    @app.route(f'{api_path}logout', methods=['POST'])
    @jwt_required()
    def logout() -> Response:
        """
        Removes a JWT cookie.
        Currently unused because cookies don't really seem to work. 

        :returns A flask response?
        """
        response = jsonify({"msg": "logout successful"})
        unset_jwt_cookies(response)
        return response

    @app.route(f'{api_path}whoami', methods=['GET'])
    @jwt_required()
    def checkJWT() -> Response:
        """
        Endpoint for testing the JWT authentication.

        :returns JSON object containing the username (on successful authentication)
        """
        return jsonify({'id': current_user.username, 'role': current_user.role}), 200

    @app.route(f'{api_path}createUser', methods=['POST'])
    @admin_required()
    def createUser() -> Response:
        """
        Creates a new user account. Admin function separate from the register endpoint.

        Takes two mandatory form parameters:
        :param username: The name of the user to create
        :param password: Password of the user to create
        :returns 200: Success, 400: Username or password missing, 409: Already exists
        """
        try:
            username = request.form.get('username', None)
            password = request.form.get('password', None)

            if not username or not password:
                return jsonify({'msg': "Either username or password missing!"}), 400
            if metaboserv.recordExists('users', username):
                return jsonify("User with that name exists already!"), 409
            return createResponse(metaboserv.createUser(username, password))
        except Exception as e:
            logging.error(e)
            logging.error(f"Failed creating a user entity: {e}")
            return createErrorResponse(mandatory_parameters=['username', 'password'])

    @app.route(f'{api_path}getUser', methods=['GET'])
    @jwt_required()
    def getUser() -> Response:
        """
        Gets user data.

        :returns 200: User object (JSON), 400: Username missing, 401: Unauthorized, 404: Not found
        """
        try:
            username = request.args.get('username', None)
            if not username:
                return jsonify({'msg': "Username is missing!"}), 400
            if current_user.username != "admin" and current_user.username != username:
                return jsonify({'msg': "You are not authorized to retrieve this user's data!"}), 401
            if not metaboserv.recordExists("users", username):
                return jsonify({'msg': f"User {username} does not exist!"}), 404
            return createResponse(metaboserv.getUser(username, current_user if current_user else None))
        except Exception:
            return createErrorResponse(mandatory_parameters=["username"])

    @app.route(f'{api_path}addContributor', methods=['POST'])
    @jwt_required()
    def addContributor() -> Response:
        """
        Adds a new contributor to a study, i.e. gives viewing and optionally management permissions to a user.
        Requires a JWT authenticating the issuing user.

        Takes form parameters:
        :param username: The name of the user to add permissions to
        :param study: The study to add permissions for
        :param manage: true if user is to get managament permissions, false otherwise
        :returns 200: Success, 400: Missing parameters, 401: Unauthorized, 403: Forbidden, 404: User to update or study not found, 409: User is already contributor
        """
        try:
            if not current_user or (current_user.username != "admin" and not current_user.permissions.manage):
                return jsonify("Issuing user has no management permissions!"), 401
            username = request.form.get('username')
            study = request.form.get('study')
            perm_type = "manage" if request.form.get('manage') and request.form.get('manage')=="true" else "view"

            if not username or not study:
                return jsonify({'msg': "Username or study missing!"}), 400
            if current_user.username != 'admin' and study not in current_user.permissions.manage:
                return jsonify({'msg': f"Management permission on study {study} is missing for the issuing user ({current_user.username})!"}), 401
            if current_user.username == username:
                return jsonify({'msg': "Users can not add themselves as contributors."}), 403
            if not metaboserv.recordExists(metaboserv.user_index, username):
                return jsonify({'msg': f"User {username} does not exist."}), 404
            if not metaboserv.recordExists(metaboserv.study_index, study):
                return jsonify({'msg': f"Study {study} does not exist."}), 404
            study_data = metaboserv.getStudyInternal(study).getBody()
            if username == study_data['uploader']:
                return jsonify({'msg': "The uploader of this study is a contributor by default."}), 409
            if study in metaboserv.getUserInternal(username).getBody()['permissions'][perm_type]:
                return jsonify({'msg': f"This permission was already granted to user {username}!"}), 409
            return createResponse(metaboserv.addUserPermission(username, study, perm_type))
        except Exception as e:
            logging.error("Failed adding permission to user entity.\n")
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['username', 'study'])

    @app.route(f'{api_path}removeContributor', methods=['POST'])
    @jwt_required()
    def removeContributor() -> Response:
        """
        Removes a contributor from a study, taking their viewing and managing permissions.
        Requires a JWT authenticating the issuing user.

        Takes form parameters:
        :param username: The name of the user to add permissions to
        :param study: The study to add permissions for
        :returns 200: Success, 400: Missing parameters, 401: Unauthorized, 403: Forbidden, 404: User to revoke or study not found, 409: User is not a contributor
        """
        try:
            if not current_user or (current_user.username != "admin" and not current_user.permissions.manage):
                return jsonify("Issuing user has no management permissions!"), 401
            username = request.form.get('username')
            study = request.form.get('study')

            if not username or not study:
                return jsonify({'msg': "Username or study missing!"}), 400
            if current_user.username != 'admin' and study not in current_user.permissions.manage:
                return jsonify({'msg': f"Management permission on study {study} is missing for the issuing user ({current_user.username})!"}), 401
            if current_user.username == username:
                return jsonify({'msg': "Users can not revoke their own access rights."}), 403
            if not metaboserv.recordExists(metaboserv.user_index, username):
                return jsonify({'msg': f"User {username} does not exist."}), 404
            if not metaboserv.recordExists(metaboserv.study_index, study):
                return jsonify({'msg': f"Study ${study} does not exist."}), 404
            study_data = metaboserv.getStudyInternal(study).getBody()
            if username == study_data['uploader']:
                return jsonify({'msg': "Permissions for the uploader of a study can not be changed."}), 403
            return createResponse(metaboserv.revokeUserPermission(username, study, "view"))
        except Exception as e:
            logging.error("Failed removing contributor from study.\n")
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['username', 'study'])

    @app.route(f'{api_path}updateContributor', methods=['POST'])
    @jwt_required()
    def updateContributor() -> Response:
        """
        Updates managing permissions on a contributor, either removing or granting them.
        Requires a JWT authenticating the issuing user.

        Takes form parameters:
        :param username: The name of the user to add permissions to
        :param study: The study to add permissions for
        :param manage: true if user is to get managament permissions, false otherwise
        :returns 200: Success, 400: Missing parameters, 401: Unauthorized, 403: Forbidden, 404: User to update or study not found, 409: User already has permission
        """
        try:
            if not current_user or (current_user.username != "admin" and not current_user.permissions.manage):
                return jsonify("Issuing user has no management permissions!"), 401
            username = request.form.get('username')
            study = request.form.get('study')
            manage = request.form.get('manage')

            if not username or not study or not manage:
                return jsonify({'msg': "Username, manage permission status or study missing!"}), 400
            if current_user.username != 'admin' and study not in current_user.permissions.manage:
                return jsonify({'msg': f"Management permission on study {study} is missing for the issuing user ({current_user.username})!"}), 401
            if current_user.username == username:
                return jsonify({'msg': "Users can not change their own permissions."}), 403
            if not metaboserv.recordExists(metaboserv.user_index, username):
                return jsonify({'msg': f"User {username} does not exist."}), 404
            if not metaboserv.recordExists(metaboserv.study_index, study):
                return jsonify({'msg': f"Study ${study} does not exist."}), 404
            study_data = metaboserv.getStudyInternal(study).getBody()
            if username == study_data['uploader']:
                return jsonify({'msg': "Permissions for the uploader of a study can not be changed."}), 403
            if manage == 'true':
                return createResponse(metaboserv.addUserPermission(username, study, "manage"))
            else:
                return createResponse(metaboserv.revokeUserPermission(username, study, "manage"))
        except Exception as e:
            logging.error("Failed adding permission to user entity.\n")
            logging.error(e)
            return createErrorResponse(mandatory_parameters=['username', 'study', 'manage'])

    @app.after_request
    def refreshJWTs(response) -> Response:
        """
        Called after each request to the service.
        Refreshes soon-expiring JWTs (30 minutes) if that JWT was used for a request.
        """
        try:
            exp_timestamp = get_jwt()["exp"]
            now = datetime.datetime.now()
            target_timestamp = datetime.datetime.timestamp(now + datetime.timedelta(minutes=30))
            if target_timestamp > exp_timestamp:
                access_token = create_access_token(identity=get_jwt_identity())
                set_access_cookies(response, access_token)
            return response
        except (RuntimeError, KeyError):
            return response

    ################################################################

    app.run(host=api_host, port=str(api_port))

    
