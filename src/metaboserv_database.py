from io import BytesIO
import logging
from typing import OrderedDict
import uuid
import numpy as np
from metaboserv_helpers import INDEXMAPPING_PHE, capitalizeChemical, process_csv_concentration_file, process_csv_phenotype_file, process_xlsx_concentration_file, extractNormalConcentrations_HMDB
from metaboserv_helpers import INDEXMAPPING_STUDY, INDEXMAPPING_SOURCE,  KNOWN_SYNONYMS, INDEXMAPPING_USER, INDEXMAPPING_MET, INDEXMAPPING_DATA, ResultObject
from elasticsearch import Elasticsearch, NotFoundError, ConflictError, AuthenticationException, AuthorizationException
from elasticsearch.helpers import scan
import os, copy
import json
import re
from collections import defaultdict
from metaboserv_plotter import MetaboservPlotter
import time
import requests
import hashlib, secrets
import datetime

logging.basicConfig(level='INFO')

class MetaboservDatabase:

    connection_uri: str  # URL for the database
    isConnected: bool  # Is the DB connection working?
    client: Elasticsearch  # Elasticsearch Python client
    upload_path: str  # Path of the upload folder
    certificate: str  # Path to ES http_ca certificate
    raw_spectra_folder_gckd_bl: str # Path where raw spectra are saved (for GCKD BL)
    study_sources_cache: dict # Cache sources for a study
    concentration_cache: dict # Cache concentration data to a limit of 10000 searches
    data_storage_folder: str # Parsed ontology data will be stored here
    nc_folder: str # Folder for storage of normal conc. data
    plot_folder: str # Folder where plots will be saved in
    generated_plots: OrderedDict # Holds a number of plots for the frontend to query (max 20, then old ones are replaced)
    generated_csv: OrderedDict # Holds generated CSV files (id:file as key-value pairs)
    plotter: MetaboservPlotter # Singleton for creating plots
    spectra_service_path: str # URL of the microservice providing raw spectra data (defaults to localhost)
    spectra_service_port: str # Port of the microservice providing raw spectra
    biospecimen: list # List of contained biospecimen
    pending_files: dict # Information about already validated files that are in the process of being uploaded
    
    study_index : str # Index holding study metadata
    user_index: str # Index holding user data
    conc_index: str # Index holding concentration data and metabolite metadata
    nconc_index: str # Index holding normal concentration values
    source_index: str # Index holding source metadata
    phenotype_index: str # Index holding phenotypical data

    """
    Singleton wrapper class for mediation between client and the ElasticSearch database.
    Parameters are mostly set from configuration file.
    """

    ################### Connection #####################

    def __init__(self, database_config: dict, connection, docker_bool):
        """
        Creates the MetaboservDatabase object.

        :param database_config: Database-related configuration from the config file
        :param connection: Holds connection details (host, connection scheme, port) - refer to API
        :param docker_bool: True if the app runs on docker, False otherwise
        """
        self.connection_uri = connection
        username = database_config['username']
        password = database_config['password']
        self.certificate = os.path.join(
            database_config['certs_path'], database_config['certs_name'])
        self.study_sources_cache = {}
        self.concentration_cache = {}
        self.pending_files = {}
        self.data_storage_folder = database_config['data_folder']
        self.nc_folder = database_config['nc_folder']
        self.plot_folder = database_config['plot_folder']
        self.generated_plots = OrderedDict()
        self.generated_csv = OrderedDict()
        self.plotter = MetaboservPlotter()
        self.spectra_service_path = database_config['spectra_path']
        self.spectra_service_port = database_config['spectra_port']

        self.biospecimen = ['serum', 'blood', 'plasma', 'urine', 'feces', 'csf']
        self.user_index = database_config['index_user']
        self.study_index = database_config['index_study']
        self.conc_index = database_config['index_conc']
        self.nconc_index = database_config['index_nconc']
        self.source_index = database_config['index_source']
        self.token_index = database_config['index_token']
        self.phenotype_index = database_config['index_phenotype']

        try:
            if docker_bool:
                time.sleep(35)
            self.isConnected = self.connectDB(username, password)
        except:
            logging.error(
                f"Failed to connect do ES with username {username} and password {password} ({self.connection_uri})")

    def connectDB(self, username: str, password: str) -> bool:
        """
        Connects to ElasticSearch.
        The mode of connection varies slightly depending on whether Docker is used or not.

        :param username: The ES username
        :param password: The ES password
        :returns True if connection established, False otherwise
        """
        try:
            self.client = Elasticsearch(
                self.connection_uri, basic_auth=(username, password))
            return True
        except AuthenticationException:
            logging.error("Failed connecting to the ElasticSearch database due to an authentication error (401).")
            return False
        except AuthorizationException:
            logging.error("Authorization exception occurred somewhere in the login process. (403)")
            return False
        except Exception as e:
            logging.error("Failed connecting to the ElasticSearch database with unknown error.")
            logging.error(e)
            return False

    ###################################################################

    # All functions that interface with flask return 'ResultObjects'.
    # Each ResultObject contains the response body (usually empty or a dict), a message (msg), and status code.
    # createResponse() is called on the ResultObjects in the API part.
    # The reasoning behind this is that it's easier to reuse the functions internally.
    # Their output can simply be used in this backend by calling getBody() on the ResultObject.

    ################### Manage DB ###################

    def cleanDB(self) -> ResultObject:
        """
        Cleans the DB completely (deletes all indices and records).
        """
        try:
            for index in [self.user_index, self.conc_index, self.study_index, self.source_index, self.nconc_index, self.token_index, self.phenotype_index,
             "other_data", "concentration_data", "metabolites", "metabolitesv2"]:
                if self.indexExists(index):
                    self.deleteRecordsByIndex(index)
                    self.deleteIndex(index)
            return ResultObject(msg="Successfully wiped the database.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during database cleaning: {e}", status=500)

    def resetDB(self, admin_pw: str) -> ResultObject:
        """
        Drops all records in the database and recreates indices.
        Also creates the admin user using the password denoted in the config file.

        :param admin_pw: The administrator password to use
        """
        try:
            for index in [self.user_index, self.conc_index, self.study_index, self.source_index, self.nconc_index, self.token_index, self.phenotype_index]:
                if self.indexExists(index):
                    self.deleteRecordsByIndex(index)
                    self.deleteIndex(index)
            self.createIndex(self.study_index, "study")
            self.createIndex(self.source_index, "source")
            self.createIndex(self.conc_index, "metabolites_and_conc")
            self.createIndex(self.user_index, "user")
            self.createIndex(self.nconc_index, "normal_conc")
            self.createIndex(self.token_index)
            self.createIndex(self.phenotype_index, "phenotype")
            self.createUser("admin", admin_pw, 'tim.tucholski@stud.uni-goettingen.de', 'UMG Goettingen', admin=True)
            return ResultObject(msg="Successfully performed a database reset.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during database reset: {e}", status=500)

    def createIndex(self, index_name: str, index_schema=None) -> ResultObject:
        """
        Creates an ElasticSearch index.

        :param index_name: The name of the index to be created
        :param index_schema: (optional) The mapping schema of the index to be created (e.g. study, source)
        """
        try:
            if self.indexExists(index_name):
                return ResultObject(msg=f"Index {index_name} already exists.", status=409)
            if index_schema == 'study':
                self.client.indices.create(index=index_name, body=INDEXMAPPING_STUDY)
            elif index_schema == "source":
                self.client.indices.create(index=index_name, body=INDEXMAPPING_SOURCE)
            elif index_schema == "user":
                self.client.indices.create(index=index_name, body=INDEXMAPPING_USER)
            elif index_schema == "metabolites_and_conc":
                    self.client.indices.create(index=index_name, body=INDEXMAPPING_MET)
            elif index_schema == "normal_conc":
                    self.client.indices.create(index=index_name, body=INDEXMAPPING_DATA)
            elif index_schema == "phenotype":
                    self.client.indices.create(index=index_name, body=INDEXMAPPING_PHE)
            else:
                self.client.indices.create(
                    index=index_name, body={"settings": {"index.mapping.total_fields.limit": 2000}}
            )
            return ResultObject(msg=f"Successfully created index {index_name}.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during index creation: {e}", status=500)

    def deleteIndex(self, index_name: str) -> ResultObject:
        """
        Deletes an ElasticSearch index.

        :param index_name: The name of the index to be deleted
        """
        try:
            if not self.indexExists(index_name):
                return ResultObject(msg=f"Index '{index_name}' does not exist.", status=404)
            self.client.indices.delete(index=index_name)
            return ResultObject(msg=f"Successfully deleted index '{index_name}'.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during index deletion: {e}", status=500)

    def getIndex(self, index_name: str) -> ResultObject:
        """
        Returns metadata about an Elasticsearch index.

        :param index_name: The name of the index to query
        :returns Index data
        """
        try:
            if not self.indexExists(index_name):
                return ResultObject(msg=f"Index '{index_name}' does not exist.", status=404)
            payload = self.client.indices.get(index=index_name)
            return ResultObject(body=payload)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting index information: {e}", status=500)

    def indexExists(self, index_name: str) -> bool:
        """
        Checks if an ElasticSearch index already exists.

        :param index_name: The name of the index to be checked
        :returns True if index exists, False otherwise
        """
        return self.client.indices.exists(index=index_name)

    def recordExists(self, index_name: str, record_id: str) -> bool:
        """
        Checks if a record exists in the ElasticSearch DB.

        :param index_name: The name of the index in question
        :param record_id: The identifier of the record in question
        :returns True if the record exists, False otherwise
        """
        return self.client.exists(index=index_name, id=record_id)

    def deleteRecord(self, index_name: str, record_id: str) -> ResultObject:
        """
        Deletes a record in ES.

        :param index_name: The name of the index the record lives in
        :param record_id: The name of the record to be deleted
        """
        try:
            self.client.delete(index=index_name, id=record_id)
            return ResultObject(msg=f"Successfully deleted record '{index_name}:{record_id}'.")
        except NotFoundError:
            return ResultObject(msg=f"Either the record '{record_id}' or the index '{index_name}' was not found.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during record deletion: {e}", status=500)

    def deleteRecordsByIndex(self, index_name: str) -> ResultObject:
        """
        Deletes all records in ES that live in a certain index.

        :param index_name: The name of the index to delete all records for
        """
        try:
            if not self.indexExists(index_name):
                return ResultObject(msg=f"Index '{index_name}' does not exist.", status=404)
            self.client.delete_by_query(index=index_name, body={
                                    "query": {"match_all": {}}})
            return ResultObject(msg=f"Successfully deleted all records in index '{index_name}'.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during record deletion: {e}", status=500)

    def createInitialCache(self) -> ResultObject:
        """
        Checks if any data is in the ES DB and caches data for studies and concentrations (up to a limit of 10000).
        """
        try:
            studies = self.getListOfStudies().getBody()
            for study in studies.keys():
                sources = self.getSourcesInStudy(study).getBody()
                for source_id in sources.keys():
                    _ = self.getConcentrationData(source_id)
            return ResultObject(msg="Cache created and filled successfully.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during cache creation: {e}", status=500)

    def getRecord(self, index_name: str, record_id: str) -> ResultObject:
        """
        Retrieves an arbitrary record from the database by index and ID.

        :param index_name: Name of the index
        :param record_id: ID of the document
        """
        try:
            return ResultObject(body=self.client.get(index=index_name, id=record_id))
        except NotFoundError:
            return ResultObject(msg="Record could not be found!", status=404)
        except Exception as e:
            logging.error(e)

    ################### Manage studies ###################

    def createStudy(self, study_dict: dict) -> ResultObject:
        """
        Creates a study record in ES.
        A study holds experimental data from at least one source (e.g. a patient).

        :param study_dict: Dict containing the study data
        """
        try:
            if self.recordExists(self.study_index, study_dict["study_id"]):
                return ResultObject(msg=f"Study '{study_dict['study_id']}' already exists.", status=409)
            self.client.index(
                index=self.study_index, id=study_dict['study_id'], body=json.dumps(study_dict))
            return ResultObject(msg=f"Successfully created study record '{study_dict['study_id']}'.", status=200)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during study record creation: {e}", status=500)

    def getStudy(self, study: str, user=None, auth_token=None) -> ResultObject:
        """
        Returns metadata about a specific study.
        Supports bulk transactions (multiple studies, semicolon-separated)

        :param study_id: The name(s) of the study/studies to query
        :param user: The user performing the query (either username or None if anonymous)
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns Study data
        """
        try:
            study_ids = study.split(';')
            if len(study_ids) == 1:
                if self.checkUserPermission(user, study_ids[0]) or (user and user.username == 'admin') or self.checkTokenPermission(auth_token, study_ids[0]):
                    return ResultObject(body=self.client.get(index=self.study_index, id=study)['_source'])
                else:
                    return ResultObject(msg="You don't have the required permissions to access this information!", status=401)
            else:
                return_dict = {}
                bulk_results = self.client.mget(index=self.study_index, body={'ids': study_ids})
                for bulk_dict in bulk_results['docs']:
                    if "_source" in bulk_dict.keys() and (self.checkUserPermission(user, bulk_dict["_id"]) or
                     user.username == 'admin' or self.checkTokenPermission(auth_token, bulk_dict["_id"])):
                        return_dict[bulk_dict["_id"]] = bulk_dict["_source"]
                if not return_dict:
                    return ResultObject(msg="You don't have the required permissions to access this information!", status=401)
                else:
                    return ResultObject(body=return_dict)
        except NotFoundError:
            return ResultObject(msg="None of the provided studies were found.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while getting study data: {e}", status=500)

    def getStudyInternal(self, study: str) -> ResultObject:
        """
        Gets study data. Only for internal use (checking permission against it)

        :param study: The name of the study/studies to query
        :returns Study data
        """
        try:
            return ResultObject(body=self.client.get(index=self.study_index, id=study)['_source'])
        except NotFoundError:
            return ResultObject(msg="None of the provided studies were found.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while getting study data: {e}", status=500)

    def updateStudyMetadata(self, study: str, study_dict: dict, user=None, auth_token=None) -> ResultObject:
        """
        Adds or updates metadata (author, date etc.) to a study in the ES database.
        Metadata must have a specific format (see API function)

        :param study: ID of the study in question
        :param study_dict: The metadata object with according key-value pairs
        :param user: User trying to add/update metadata
        :param auth_token: Auth. token that can be used separately from JWT authentication
        """
        try:
            if not self.checkUserPermission(user, study) and not self.checkTokenPermission(auth_token, study, "manage"):
                return ResultObject(msg=f"You lack permission to add or update metadata for this study!", status=401)
            if not self.recordExists(self.study_index, study):
                return ResultObject(msg=f"Study '{study}' was not found in the database.", status=404)
            current_study_data = self.getStudyInternal(study).getBody()
            for key in study_dict.keys():
                current_study_data[key] = study_dict[key]
            self.client.update(index=self.study_index, id=study, body={"doc": current_study_data})
            return ResultObject(msg=f"Successfully updated metadata for study {study}.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while updating metadata for study '{study}':{e}", status=500)

    def getListOfStudies(self, user=None, auth_token=None) -> ResultObject:
        """
        Get a list of all studies that are stored in the ES instance.

        :param user: The user performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns List of studies
        """
        try:
            logging.info(auth_token)
            study_scan = scan(self.client, index="studies", query={
                                    "query": {"match_all": {}}})
            study_dict = {}
            for k in study_scan:
                study_details = k["_source"]
                if study_details['visibility'] == 'private':
                    if not user and not auth_token: continue
                    if not self.checkTokenPermission(auth_token, k["_id"]) and not self.checkUserPermission(user, k["_id"]): continue
                    study_dict[k["_id"]] = study_details
                else:
                    study_dict[k["_id"]] = study_details
            return ResultObject(body=study_dict)
        except Exception as e:
            return ResultObject(msg="Failed to retrieve study list.", status=500)

    ################### Manage sources ###################

    def createSource(self, source_dict: dict) -> ResultObject:
        """
        Creates a source record in ES.
        A "source" refers to any source of metabolomics data, e.g. a patient ID from a particular study.

        :param study_dict: Dict containing the study data
        """
        try:
            if source_dict["source_id"] in self.study_sources_cache.keys() or self.recordExists("sources", source_dict["source_id"]):
                return ResultObject(msg=f"Source record '{source_dict['source_id']}' already exists.", status=409)
            self.client.index(
                index=self.source_index, id=source_dict['source_id'], body=json.dumps(source_dict))
            return ResultObject(msg=f"Successfully created source record '{source_dict['source_id']}'.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during source record creation: {e}", status=500)

    def getSource(self, source: str, user=None, auth_token=None) -> ResultObject:
        """
        GETs a source from ES and returns it as a dict.

        :param source: The name of the source to GET
        :param user: The user performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns Source information
        """
        try:
            source_data = self.client.get(index=self.source_index, id=source)['_source']
            source_study = source_data['associated_studies']
            if type(source_study) == str: # Only one study associated
                if not self.checkUserPermission(user, source_study) and not self.checkTokenPermission(auth_token, study):
                    return ResultObject(msg="You don't have the required permissions to access this information!", status=401)
            elif type(source_study) == list:
                studies_left_over = []
                for study in source_study:
                    if self.checkUserPermission(user, study) or self.checkTokenPermission(auth_token, study): studies_left_over.append(study)
                if not studies_left_over:
                    return ResultObject(msg="You don't have the required permissions to access this information!", status=401)
                else:
                    source_data['associated_studies'] = studies_left_over
            return ResultObject(body=source_data)
        except NotFoundError:
            return ResultObject(msg=f"Provided source '{source}' was not found.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting source information: {e}", status=500)

    def getSourcesInStudy(self, study: str, user=None, auth_token=None) -> ResultObject:
        """
        Retrieves a list of all sources (e.g. patients) that belong to a specific study.

        :param study: Name of the study to search in
        :param user: The user performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns List of all sources that belong to a study
        """
        try:
            if study in self.study_sources_cache.keys() and (self.checkUserPermission(user, study) or self.checkTokenPermission(auth_token, study)):
                match_dict = self.study_sources_cache.get(study)
            else:
                if (not self.checkUserPermission(user, study) and not self.checkTokenPermission(auth_token, study)):
                    return ResultObject(msg=f"You don't have the required permissions to access the sources in study '{study}'.", status=401)
                if not self.recordExists(self.study_index, study):
                    return ResultObject(msg=f"Study {study} does not exist.", status=404)
                match_dict = {}
                study_sources = scan(self.client, index=self.source_index, query={"query": {"match": {"associated_studies": str(study)}}})

                for matching_source in study_sources:
                    match_dict[matching_source["_source"]["source_id"]] = matching_source["_source"]
                self.study_sources_cache[study] = match_dict
            return ResultObject(body=match_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting all sources in study '{study}': {e}", status=500)

    def getConcentrationData(self, source_id: str, user=None, auth_token=None) -> ResultObject:
        """
        Retrieves concentration data for a particular source (~patient).
        To be used sparsely as this is fairly heavy on database load.
        To query conc. data for more than one patient, use filterByMetabolites (with mode=full for complete results).

        :param source_id: Identifier for the source
        :param user: User performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns Aggregated concentration data
        """
        try:
            source_data = self.getSource(source_id, user={'username': 'admin'}).getBody() #replacement for internal getSource method
            src_dict = defaultdict(dict)
            for study in source_data['associated_studies']:
                if not self.checkUserPermission(user, study) and not self.checkTokenPermission(auth_token, study): continue
                src_dict[study] = {}
                cnc_query = {"query":{"bool":{"must":[
                    {"has_parent":{"parent_type": "metabolite", "query": {'match_all': {}}}},
                    {"term":{"study": study}}
                ]}}}
                results = scan(self.client, index=self.conc_index, query=cnc_query)
                for collection in results:
                    if source_id in collection['_source']['concentration'].keys():
                        src_dict[study][collection['_source']['metabolite']] = collection['_source']['concentration'][source_id]
            return ResultObject(body=src_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while getting concentration data for '{source_id}': {e}", status=500)

    ################### Manage metabolites ###################

    def addMetabolite(self, metabolite: str, meta_from: str, meta_cat: str, study: str, unit: str, additional_info={}) -> ResultObject:
        """
        Adds the metabolite to a special index in ES that just keeps a metabolite list.

        :param metabolite: The name of the metabolite
        :param meta_from: Serves for a very open characterization of metabolites (lipid, non-lipid)
        :param meta_cat: Finer category for metabolites (e.g. amino acids, bile acids..)
        :param study: Study the metabolite was investigated in
        :param unit: Unit the metabolite was measured in
        :param additional_info: Additional information that might be relevant (e.g. shorthand, alternate names)
        """
        try:
            if self.recordExists(self.conc_index, metabolite or metabolite in KNOWN_SYNONYMS.keys()):
                return ResultObject(msg=f"Metabolite '{metabolite}' already exists in the database ('{metabolite}'').", status=409)
            if 'short' in additional_info.keys() and metabolite.upper().replace('_', ' ') == additional_info['short']:
                metabolite_n =  metabolite.upper().replace('_', ' ') # Unnamed lipids
            else:
                metabolite_n = capitalizeChemical(metabolite.replace("_", " ")) # Any other metabolite
            meta_dict = {
                "metabolite_name": metabolite_n,
                "metabolite_max": 0,
                "entry_type": "metabolite",
                "super_category": meta_from,
                "category": meta_cat,
                "studies": [study] if study!="None" else [],
                "unit": unit,
                "id": metabolite,
                "pc_relation": {
                    "name": "metabolite",
                }
            }
            for key,value in additional_info.items():
                meta_dict[key] = value
            self.client.index(index=self.conc_index, id=metabolite.replace(":","_"), body=json.dumps(meta_dict))
            return ResultObject(msg=f"Successfully added metabolite '{metabolite}' to database.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while adding metabolite '{metabolite}': {e}", status=500)

    def getMetabolite(self, meta_id: str, user=None, auth_token=None) -> ResultObject:
        """ 
        GETs information about a metabolite from ES and returns it as a dict.

        :param meta_id: Name of the metabolite
        :param user: The user performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns Metabolite information
        """
        try:
            if meta_id in KNOWN_SYNONYMS.keys():
                meta_id = KNOWN_SYNONYMS[meta_id]
            metabolite_data = self.client.get(index=self.conc_index, id=meta_id.lower().replace(' ', '_').replace(':', '_'))['_source']
            metabolite_data['studies'] = [x for x in metabolite_data['studies'] if (self.checkUserPermission(user, x) or self.checkTokenPermission(auth_token, x))]
            metabolite_data['metabolite_name'] = metabolite_data['metabolite_name']
            if 'short' in metabolite_data.keys():
                metabolite_data['short'] = metabolite_data['short']
            return ResultObject(body=metabolite_data)
        except NotFoundError:
            try:
                metabolite_query = {"query":{"bool": {"should": [
                    {"term": {"short": meta_id}},
                    {"term": {"metabolite_name": meta_id.replace('_', ' ')}},
                    {"term": {"alternate_name": meta_id.replace('_', ' ')}},
                    {"term": {"alternate_short": meta_id}}
                    ]
                }}}
                metabolite_scan = scan(self.client, index=self.conc_index, query=metabolite_query)
                for item in metabolite_scan:
                    metabolite_data = item['_source']
                    metabolite_data['studies'] = [x for x in metabolite_data['studies'] if (self.checkUserPermission(user, x) or self.checkTokenPermission(auth_token,x))]
                    metabolite_data['metabolite_name'] = metabolite_data['metabolite_name']
                    if 'short' in metabolite_data.keys():
                        metabolite_data['short'] = metabolite_data['short']
                    return ResultObject(body=metabolite_data)
                return ResultObject(msg=f"Provided metabolite '{meta_id}' was not found.", status=404)
            except Exception as e:
                logging.error(e)
                return ResultObject(msg=f"Encountered an exception during metabolite query fallback: {e}", status=500)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting metabolite information: {e}", status=500)

    def getMetaboliteInternal(self, meta_id: str) -> ResultObject:
        """ 
        GETs information about a metabolite from ES and returns it as a dict.
        For internal use (not accessible through API)

        :param meta_id: Name of the metabolite
        :returns Metabolite information
        """
        try:
            if meta_id in KNOWN_SYNONYMS.keys():
                meta_id = KNOWN_SYNONYMS[meta_id]
            metabolite_data = self.client.get(index=self.conc_index, id=meta_id.lower().replace(' ', '_').replace(':', '_'))['_source']
            metabolite_data['metabolite_name'] = metabolite_data['metabolite_name']
            if 'short' in metabolite_data.keys():
                metabolite_data['short'] = metabolite_data['short']
            return ResultObject(body=metabolite_data)
        except NotFoundError:
            try:
                metabolite_query = {"query":{"bool": {"should": [
                    {"term": {"short": meta_id}},
                    {"term": {"metabolite_name": meta_id.replace('_', ' ')}},
                    {"term": {"alternate_name": meta_id.replace('_', ' ')}},
                    {"term": {"alternate_short": meta_id}}
                    ]
                }}}
                metabolite_scan = scan(self.client, index=self.conc_index, query=metabolite_query)
                for item in metabolite_scan:
                    metabolite_data = item['_source']
                    metabolite_data['metabolite_name'] = metabolite_data['metabolite_name']
                    if 'short' in metabolite_data.keys():
                        metabolite_data['short'] = metabolite_data['short']
                    return ResultObject(body=metabolite_data)
                return ResultObject(msg=f"Provided metabolite '{meta_id}' was not found.", status=404)
            except Exception as e:
                logging.error(e)
                return ResultObject(msg=f"Encountered an exception during metabolite query fallback: {e}", status=500)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting metabolite information: {e}", status=500)

    def updateMetabolite(self, metabolite: str, data: dict) -> ResultObject:
        """
        Allows changing single fields of a metabolite.
        Mostly useful for when not the entire metabolite source data has to be parsed.

        :param metabolite: Identifier of the metabolite
        :param data: Key-value pairs to update, in a dictionary
        """
        try:
            if not self.recordExists(self.conc_index, metabolite):
                return ResultObject(msg=f"Metabolite '{metabolite}' was not found in the database.", status=404)
            current_met_data = self.getMetabolite(metabolite).getBody()
            for key in data.keys():
                current_met_data[key] = data[key]
            self.client.update(index=self.conc_index, id=metabolite, body={"doc": data})
            return ResultObject(msg=f"Successfully added or changed data for metabolite {metabolite}.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during metabolite field update: {e}", status=500)

    def getListOfMetabolites(self, user=None, auth_token=None) -> ResultObject:
        """
        Returns a list of all metabolites that are stored in the ES instance and recorded in at least one study.

        :param user: The user performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns List of metabolites
        """
        try:
            metabolite_scan = scan(self.client, index=self.conc_index, query={
                                    "query": {"match": {"entry_type": "metabolite"}}})
            meta_dict = {}
            for k in metabolite_scan:
                if k["_id"] not in KNOWN_SYNONYMS.keys():
                    meta_studies = [x for x in k["_source"]['studies'] if self.checkUserPermission(user, str(x)) or self.checkTokenPermission(auth_token, x)]
                    if not meta_studies:
                        continue
                    meta_dict[k["_id"]] = k["_source"]
                    meta_dict[k["_id"]]['studies'] = meta_studies
            return ResultObject(body=meta_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while getting metabolite list: {e}", status=500)

    def getListOfMetabolitesForStudy(self, study: str, user=None, mode="partial", auth_token=None) -> ResultObject:
        """
        Returns a list of all metabolites that are stored in the ES instance.
        Only returns metabolites that are recorded in a particular study.

        :param study: Study identifier
        :param user: The user account performing the request
        :param mode: If 'partial', only returns values required for frontend. If 'full', returns everything.
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns List of metabolites
        """
        try:
            if not self.checkUserPermission(user, study) and not self.checkTokenPermission(auth_token, study):
                return ResultObject(msg="You don't have the required permissions to query this data!", status=401)
            metabolite_scan = scan(self.client, index=self.conc_index, query={
                "query": { "bool": { "must":[
                    {"match": {"study": str(study)}},
                    {"term": {"entry_type": "source"}},
                    ]}}})
            meta_dict = {}
            # Metabolite List
            for k in metabolite_scan:
                if k["_id"] not in KNOWN_SYNONYMS.keys():
                    if mode == 'full':
                        meta_dict[k["_id"]] = k["_source"]
                        meta_dict[k["_id"]]['metabolite_name'] = meta_dict[k["_id"]]['metabolite']
                        del meta_dict[k["_id"]]['metabolite']
                    elif mode == 'partial':
                        meta_dict[k["_id"]] = {}
                        meta_dict[k["_id"]]['metabolite_name'] = capitalizeChemical(k["_source"]['metabolite'])
                        meta_dict[k["_id"]]['unit'] = k["_source"]['unit']
                        meta_dict[k["_id"]]['coverage'] = k["_source"]['coverage']
                        meta_dict[k["_id"]]['min'] = k["_source"]['min_value']
                        meta_dict[k["_id"]]['max'] = k["_source"]['max_value']

            return ResultObject(body=meta_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while getting metabolite list: {e}", status=500)

    def addStudyToMetabolite(self, metabolite: str, study: str) -> ResultObject:
        """
        Adds a study ID to a metabolite, indicating that the metabolite was checked for in the study.

        :param metabolite: The identifier of the metabolite
        :param study: The identifier of the study
        """
        try:
            metabolite_normalized = metabolite.lower().replace(" ", "_")
            if not self.recordExists(self.conc_index, metabolite_normalized):
                return ResultObject(msg=f"Metabolite '{metabolite_normalized}' does not exist in the database.", status=404)
            metabolite_current = self.getMetaboliteInternal(metabolite_normalized).getBody()
            if study in metabolite_current["studies"]:
                return ResultObject(msg=f"Study {study} was already present on metabolite.")
            else:
                if isinstance(metabolite_current["studies"], list):
                    studies_updated = metabolite_current["studies"] + [study]
                else:
                    studies_updated = [metabolite_current["studies"]] + [study]
                self.client.update(index=self.conc_index, id=metabolite_normalized, body={"doc": {"studies": studies_updated}})
                return ResultObject(msg=f"Successfully added study '{study}' to the metabolite '{metabolite}'.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception trying to add study '{study}' to metabolite '{metabolite}': {e}", status=500)

    def getNormalConcentration(self, metabolites: list, specimen: list, age: list, sex: str, units: list, source: list):
        """
        Retrieves normal concentration ranges from the database.
        Considers a subset of metabolites, specimen, agegroups and units (if provided)
        The sources (e.g. hmdb) can also be specified
        """
        try:
            if not specimen: specimen = self.biospecimen
            nconc_query = {"query": {
                "bool": {
                    "must": [
                        {"terms": {
                            "metabolite": metabolites,
                        }},
                        {"terms": {
                            "specimen": specimen
                        }},
                    ]
                }
            }}
            if sex != "not specified": nconc_query["query"]["bool"]["must"].append({"term": {"sex": sex}})
            if units: nconc_query["query"]["bool"]["must"].append({"terms": {"unit": units}})
            if units: nconc_query["query"]["bool"]["must"].append({"terms": {"age": age}})
            if source: nconc_query["query"]["bool"]["must"].append({"terms": {"source": source}})
            normalconc_candidates = scan(self.client, index=self.nconc_index, query=nconc_query)
            results = defaultdict(dict)
            for collection in normalconc_candidates:
                results[collection['_id']] = collection['_source']
            results = dict(sorted(results.items(), key=lambda x: (x[1]['sex'], x[1]['age'], x[1]['condition'], x[1]['min'], x[1]['max'])))
            return ResultObject(body=results)
        except Exception as e:
            logging.error(e)

    ################### Phenotype data ###################

    def getPhenotypeData(self, phenotypes=None, studies=None, user=None, auth_token=None) -> ResultObject:
        """
        Retrieves phenotype data. Filtering is based on the parameters. They should always be passed as lists/iterables.

        :param phenotypes: Phenotypes to consider
        :param studies: Studies to consider
        :param user: User performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns Phenotype data (or empty dict - based on filtering)
        """
        try:
            if not phenotypes and not studies:
                return ResultObject(msg="You must provide at least one search parameter!", status=400)
            search_query = {"query":{"bool":{"must":[
                {"term":{"entry_type": "data"}},
            ]}}}
            if phenotypes:
                search_query['query']['bool']['must'].append(({"terms":{"phenotype_id": phenotypes}}))
            if studies:
                search_query['query']['bool']['must'].append(({"terms":{"study": studies}}))
            result_dict = defaultdict(dict)
            results = scan(self.client, index=self.phenotype_index, query=search_query)
            for res in results:
                res_study = res['_source']['study']
                if self.checkUserPermission(user, res_study) or self.checkTokenPermission(auth_token, res_study):
                    result_dict[res_study][res['_source']['phenotype_id']] = res['_source']
            return ResultObject(body=result_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg="Encountered an error while retrieving phenotype data.", status=500)

    def getPhenotypeMetadata(self, study: str, source_ids=None, user=None, auth_token=None) -> ResultObject:
        """
        Retrieves metadata about a specific phenotype occurrence - i.e. specific study and source (~patient).
        General metadata about a phenotype (e.g. diseases) is currently not contained.

        :param study: The study to consider
        :param source_ids: List of source identifiers to consider, e.g. list of patients to get metadata about a phenotype for
        :param user: User performing the request
        :param auth_token: Auth. token that can be used separately to JWT authentication
        """
        try:
            if not self.checkUserPermission(user, study) and not self.checkTokenPermission(auth_token, study):
                return ResultObject(msg="You don't have permission to make queries about this study!", status=401)
            search_query = {"query":{"bool":{"must":[
                {"has_parent":{"parent_type": "phenotype_study","query":{"match":{"study": study}}}},
                {"term":{"entry_type": "metadata"}},
                {"terms": {"associated_source": source_ids if isinstance(source_ids, list) else [source_ids]}}
            ]}}}
            logging.info(search_query)
            result_dict = defaultdict(dict)
            result_dict['study'] = study
            results = scan(self.client, index=self.phenotype_index, query=search_query)
            for res in results:
                result_dict[res["_source"]['associated_source']] = res["_source"]
            return ResultObject(body=result_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg="Encountered an error while retrieving phenotype metadata for a source.", status=500)

    def getPhenotypeList(self, studies=None, user=None, auth_token=None) -> ResultObject:
        """
        Retrieves a list of phenotypes and the relevant data.
        If studies are given, only considers the provided studies.
        Otherwise all studies that can be queried based on the user and auth token (if provided)
        
        :param studies: List of studies to consider (can be empty)
        :param user: User performing the request
        :param auth_token: Auth. token that can be used separately from JWT authentication.
        :returns List of applicable phenotypes with corresponding metadata
        """
        try:
            if not studies or len(studies) == 0:
                studies = list(self.getListOfStudies(user=user, auth_token=auth_token).getBody().keys())
            else:
                studies = list(filter(lambda x: self.checkUserPermission(user,x) or self.checkTokenPermission(auth_token,x), studies))
            search_query = {"query":{"bool":{"must":[
                {"term":{"entry_type": "data"}},
                {"terms":{"study": studies}},
            ]}}}
            result_dict = defaultdict(dict)
            results = scan(self.client, index=self.phenotype_index, query=search_query)
            for res in results: 
                res['_source']['number_sources'] = len(res['_source']['data'].keys())
                del res['_source']['data']
                pid = res['_source']['phenotype_id']
                if pid not in result_dict.keys():
                    result_dict[pid] = {
                        'phenotype_id': pid,
                        'phenotype_raw': res['_source']['phenotype_raw'],
                        'levels': {},
                        'studies': [],
                        'number_sources': [],
                    }
                result_dict[pid]['levels'][res['_source']['study']] = res['_source']['levels']
                result_dict[pid]['number_sources'].append(str(res['_source']['number_sources']) + f" ({res['_source']['study']})")
                if res['_source']['study'] not in result_dict[pid]['studies']:
                    result_dict[pid]['studies'].append(res['_source']['study'])
            return ResultObject(body=result_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an error while retrieving phenotype list: {e}", status=500)

    ################### Manage user accounts ###################

    def createUser(self, username: str, password: str, email: str, inst: str, admin=False) -> ResultObject:
        """
        Creates a user entity in ES. Invoked through "Register" button in the frontend.
        The password is salted and only stored in a hashed form (for verification purposes).

        :param username: The name of the user to create
        :param password: The password of the user
        :param email: The e-mail address of the user
        :param inst: The institution of the user (e.g. university)
        :param admin: True for creation of admin user (only the very first user account created automatically)
        """
        try:
            ### These are mainly a failsafe, it should already be ensured this can't happen through the API ###
            if self.recordExists(self.user_index, username):
                return ResultObject(msg=f"User with that name ('{username}') already exists!", status=409)
            if username == 'public':
                return ResultObject(msg=f"The name 'public' is protected and can not be used.", status=400)
            if not admin and username == 'admin':
                return ResultObject(msg=f"The name 'admin' is protected and can not be used.", status=400)

            salt = secrets.token_hex(64)
            password_enc = password.encode()
            digest = hashlib.pbkdf2_hmac('sha256', password_enc, salt.encode(), 10000)
            user_dict = {
                'username': username,
                'password': digest.hex(),
                'salt': salt,
                'email': email,
                'institution': inst,
                'role': "user",
                'permissions': {
                    'manage': [],
                    'view': []
                }
            }
            if username == "admin":
                user_dict['role'] = "admin"
            self.client.index(
                index=self.user_index, id=user_dict['username'], body=json.dumps(user_dict))
            return ResultObject(msg=f"Successfully created user record '{username}'. You can now log in!")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during account creation: {e}", status=500)

    def getUser(self, username: str, user=None) -> ResultObject:
        """
        Gets data for a specific user from the ElasticSearch database.
        For public use (gated behind user permissions - users can only query their own data).
        Salt and password are not included unless its specifically an admin request.

        :param username: The name of the user
        :param user: The user account performing the request
        :returns User data
        """
        try:
            user_dict = self.client.get(index=self.user_index, id=username)['_source']
            if user.username != "admin":
                del user_dict['salt']
                del user_dict['password']
            return ResultObject(body=user_dict)
        except NotFoundError:
            return ResultObject(msg="User with provided name does not exist.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting user information: {e}", status=500)

    def getUserInternal(self, username: str) -> ResultObject:
        """
        Gets data for a specific user from the ElasticSearch database.
        For internal use (not reachable through API).

        :param username: The name of the user
        :returns User data
        """
        try:
            user_dict = self.client.get(index=self.user_index, id=username)['_source']
            return ResultObject(body=user_dict)
        except NotFoundError:
            return ResultObject(msg="User with provided name does not exist.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting user information: {e}", status=500)

    def checkUser(self, username: str, password: str) -> object or None:
        """
        Checks if a user exists and if the provided password matches.

        :param username: The provided username
        :param password: The provided password
        :returns User object (on success), None otherwise
        :remarks We can't return a ResultObject here as to comply with flask-jwt-extended
        """
        try:
            user_in_question = self.getUserInternal(username).getBody()
            encoded_password = password.encode()
            hashed_password = hashlib.pbkdf2_hmac('sha256', encoded_password, user_in_question['salt'].encode(), 10000).hex()
            if (user_in_question['username'] == username and user_in_question['password'] == hashed_password):
                return user_in_question
            else: return
        except Exception as e:
            logging.error(e)
            return

    def addUserPermission(self, username: str, study: str, type="view") -> ResultObject:
        """
        Adds permissions for a user (view/manage) and a specific study.
        View means they can query the database. Manage means they can manage access and delete the study.

        :param username: The name of the user
        :param study: The study to manage access rights for
        :param type: Grants managing and viewing permissions if type is "manage". Always grants viewing permissions.
        """
        try:
            if not self.recordExists(self.user_index, username):
                return ResultObject(msg=f"User '{username}' does not exist.", status=404)
            if not self.recordExists(self.study_index, study):
                return ResultObject(msg=f"Study'{study}' does not exist.", status=404)

            current_user_data = self.getUserInternal(username).getBody()
            current_perms = current_user_data["permissions"]

            if not current_perms["view"]:
                current_perms["view"] = [study]
            elif study not in current_perms["view"]:
                current_perms["view"] = current_perms["view"] + [study]

            if type == "manage" and not current_perms["manage"]:
                current_perms["manage"] = [study]
            elif type == "manage" and study not in current_perms["manage"]:
                current_perms["manage"] = current_perms["manage"] + [study]

            current_user_data["permissions"] = current_perms
            self.client.update(index=self.user_index, id=username, body={"doc": current_user_data})
            logging.info(f"Updated permissions for user {username}, user permissions are now:")
            logging.info(current_perms)
            return ResultObject(msg=f"Successfully added permission for user {username}.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while adding permissions: {e}", status=500)

    def revokeUserPermission(self, username: str, study: str, type="manage") -> ResultObject:
        """
        Revokes a user permission on a specific study.
        If type is set to manage, only removes managing permissions.
        Otherwise, completely removes the user as a contributor.

        :param username: The user account to revoke permission(s) for
        :param study: The study the permission revolves around
        """
        try:
            if not self.recordExists(self.user_index, username):
                return ResultObject(msg=f"User '{username}' does not exist.", status=404)
            if not self.recordExists(self.study_index, study):
                return ResultObject(msg=f"Study'{study}' does not exist.", status=404)

            current_user_data = self.getUserInternal(username).getBody()
            current_perms = current_user_data["permissions"]

            if current_perms["manage"] and study in current_perms["manage"]:
                current_perms["manage"].remove(study)

            if  type == "view" and current_perms["view"] and study in current_perms["view"]:
                current_perms["view"].remove(study)

            current_user_data["permissions"] = current_perms
            self.client.update(index=self.user_index, id=username, body={"doc": current_user_data})
            logging.info(f"Updated permissions for user {username}, user permissions are now:")
            logging.info(current_perms)
            return ResultObject(msg=f"Successfully revoked permissions for user {username}.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while revoking permissions: {e}", status=500)

    def checkUserPermission(self, user: object or None, study: str, type="view") -> bool:
        """
        Checks if a user has viewing or managing permissions for a certain set of studies.

        :param user: The user object, or just None if no JWT is present
        :param study: ID of the study to check
        :param permtype: Either "view" or "manage". Checks the according permission
        :returns Boolean (yes/no)
        """
        try:
            study_in_question = self.getStudyInternal(study).getBody()
            if study_in_question['visibility'] == 'public' and type == 'view':
                return True
            if not user: return False
            if user.username == 'admin': return True
            if not user.permissions: return False
            if type == 'view':
                if not user.permissions.view: return False
                if (study not in user.permissions.view and study != user.permissions.view): return False
                return True
            elif type == 'manage':
                if not user.permissions.manage: return False
                if (study not in user.permissions.manage and study != user.permissions.manage): return False
                return True
            return False
        except Exception as e:
            logging.error(e)
            return False

    def getUserList(self) -> ResultObject:
        """
        Gets a list of all users and their respective data (minus password/salt).
        Used internally, e.g. for finding all users that have access to a certain study.
        """
        try:
            user_scan = scan(self.client, index=self.user_index, query={
                                    "query": {"match_all": {}}})
            user_dict = defaultdict(dict)
            for user in user_scan:
                temp_dict = user["_source"]
                del temp_dict['password']
                del temp_dict['salt']
                user_dict[user["_id"]] = temp_dict
            return ResultObject(body=user_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during user list query: {e}", status=500)

    def getPermittedUsers(self, study: str, jwt_user=None, auth_token=None) -> ResultObject:
        """
        Going from the user list, gathers a subset of users with viewing or managing permissions for a study.

        :param study: Identifier for the study in question
        :param jwt_user: User performing the request (jwt_ added for clarity's sake)
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :returns List of users with viewing/managing access
        """
        try:
            if not self.checkUserPermission(jwt_user, study) and not self.checkTokenPermission(auth_token, study) and jwt_user.username != 'admin':
                return ResultObject(msg="You don't have the required permissions to access this information!", status=401)
            self.client.indices.refresh(index=self.user_index)
            users = self.getUserList().getBody()
            permissions = defaultdict(dict)
            for user in users.keys():
                if user == 'admin': continue
                if not users[user]['permissions']: continue
                if study in users[user]['permissions']['view']:
                    permissions[user] = {
                        'view': 1,
                        'manage': 0,
                        'username': user,
                    }
                else: continue
                if study in users[user]['permissions']['manage']:
                    permissions[user]['manage'] = 1
            return ResultObject(body=permissions)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during study permission query: {e}", status=500)         

    ################### Token Auth ##################

    def generateToken(self, study: str or list, expiration=-1, perm_type="view", comment=None, user=None) -> ResultObject:
        """
        Generates a new permission token for a study.
        The token itself is generated using the secrets library.
        Permissons (viewing or managing rights) are associated with a token.

        :param study: The study/studies to create a token for.
        :param expiration: Expiration date (DD-MM-YY or 'never')
        :param perm_type: Type of permission for the token
        :param user: The user creating the token (must not be None)
        """
        try:
            if not user or not user.username: return ResultObject(msg="You need to be logged in to create a token!", status=401)
            if not study: return ResultObject(msg="You need to provide at least one study!", status=400)
            token_id = secrets.token_hex(8)
            if isinstance(study,str): study = [study]
            if expiration == -1:
                expiration_date = "never"
            else:
                expiration_date = (datetime.datetime.today() + datetime.timedelta(days=expiration)).strftime('%d/%m/%y')
            token_data = {'author': user.username, 'permission': perm_type, 'studies': study, 'expiration': expiration_date, 'id': token_id}
            if comment: token_data['comment'] = comment
            self.client.index(index=self.token_index, id=token_id, body=token_data, refresh='wait_for')
            return ResultObject(body={'id': token_id},msg=f"Successfully created token {token_id}")
        except ConflictError:
            self.generateToken(study, perm_type, user)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during token generation: {e}", status=500)

    def getTokenData(self, token_id: str) -> ResultObject:
        """
        Retrieves token data (author, permissions, studies) by ID.

        :param token_id: The token in question
        :returns Token data
        """
        try:
            return ResultObject(body=self.client.get(index=self.token_index, id=token_id))
        except NotFoundError:
            return ResultObject(msg=f"Token {token_id} could not be found.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during token data retrieval: {e}", status=500) 

    def revokeToken(self, token_id: str, user=None) -> ResultObject:
        """
        Deletes a token.

        :param token_id - ID of the token to delete
        :user - User performing the deletion request
        """
        try:
            if not token_id: return ResultObject(msg="Token ID is missing!", status=400)
            if not user or not user.username: return ResultObject(msg="You need to be logged in to manage tokens!", status=401)
            if not self.checkTokenManagementPermission(token_id, user):
                return ResultObject(msg="You don't have the necessary permission to manage this token!", status=401)
            if not self.recordExists(self.token_index, token_id): return ResultObject(msg=f"Token {token_id} does not exist!", status=404)
            self.client.delete(index=self.token_index, id=token_id)
            return ResultObject(msg=f"Successfully deleted token {token_id}.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during token deletion: {e}", status=500) 

    def checkTokenManagementPermission(self, token_id: str, user: object) -> bool:
        """
        Checks whether a specific user can manage (e.g. delete) a specific token.

        :param token_id: The token in question
        :param user: The user performing the deletion request
        :returns True if the user has management permissions, False otherwise
        """
        try:
            if user.username == 'admin': return True
            token_data = self.getTokenData(token_id).getBody()
            if not token_data: return False
            if user.username == token_data['author']: return True
            return False
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during token management perm. check: {e}", status=500) 
        
    def checkTokenPermission(self, token_id: str, study: str, permission="view") -> bool:
        """
        Retrieves token data and checks if it covers the requested study.
        Also checks the expiration date and automatically deletes the token in that case.

        :param token_id: The token in question
        :param study: The study to check permission for
        :param permission: If "view", checks for viewing permissions. If "manage", checks for managing permissions.
        :returns True if token covers study, False otherwise
        """
        try:
            if not token_id or not study: return False
            study_data = self.getStudyInternal(study).getBody()
            if not study_data: return False
            if study_data['visibility'] == 'public': return True
            token_data = self.getTokenData(token_id).getBody()['_source']
            if not token_data:
                return False
            if (isinstance(token_data['studies'], str) and study == token_data['studies']) or study in token_data['studies']:
                if permission == "view" or (permission == "manage" and token_data['permission'] == "manage"):
                    if not self.checkTokenExpired(token_data['expiration']): return True
                    else: self.client.delete(index=self.token_index, id=token_id)
            return False
        except NotFoundError:
            logging.error(f"Non-existant token was checked: {token_id}")
            return False
        except Exception as e:
            logging.error(e)
            return False

    def checkTokenExpired(self, expiration: str or datetime.datetime) -> bool:
        """
        Checks a given expiration date against the current date.

        :param expiration: The expiration date to check (str or datetime)
        :returns True if expired, False otherwise
        """
        try:
            if isinstance(expiration, str):
                if expiration == 'never': return False
                else:
                    try:
                        date = datetime.strptime(expiration, '%d/%m/%y')
                    except ValueError as e:
                        logging.error(f"Encountered an exception during date parsing: {e}")
                        return False
            else:
                date = expiration
            current_date = datetime.datetime.now()
            if (date + datetime.timedelta(days=1)) < current_date: return True
            return False
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during token expiration date check: {e}", status=500)

    def getTokenList(self, username: str, study: str or None, user=None) -> ResultObject:
        """
        Retrieves a list of all tokens by a specific user.

        :param username: User that is being queried for their tokens
        :param user: The user performing the request (must by default be equal to queried user unless admin)
        :returns Dict of tokens ordered by studies
        """
        try:
            if not user: return ResultObject(msg="You must be logged in to retrieve a list of tokens!", status=401)
            if not username: return ResultObject(msg="No user to check provided!", status=400)
            if user.username != 'admin' and username != user.username: return ResultObject(msg="You don't have permissions to access this user's tokens!", status=401)
            token_list = []
            token_query = {"query": {"match": {"author": username}}}
            self.client.indices.refresh(index=self.token_index)
            token_results = scan(self.client, index=self.token_index, query= token_query)
            for res in token_results:
                for maybe_study in res['_source']['studies']:
                    if study and maybe_study != study: continue 
                    token_list.append(res['_source'])
            return ResultObject(body=token_list)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during token list retrieval: {e}", status=500)

    ################### File parsing ##################

    def addToPendingFileList(self, study: str, filepaths: list, extensions: list) -> bool:
        """
        Adds information to the "pending file list", which is actually a dict.
        It holds metadata about files that are already uploaded and validated, but not yet processed.
        After processing, the entry should be deleted.

        :param study: The study the files are for
        :param filepaths: Paths to each file
        :param extensions: File extensions (for simplicity's sake)
        :returns Successful (True/False)?
        """
        try:
            if study in self.pending_files.keys():
                return False
            self.pending_files[study] = {
                'datafile': {
                    'path': filepaths[0],
                    'extension': extensions[0]
                }
            }
            if filepaths[1] and extensions[1]:
                self.pending_files[study]['mdfile'] = {
                    'path': filepaths[1],
                    'extension': extensions[1]
                }
            if filepaths[2] and extensions[2]:
                self.pending_files[study]['phenofile'] = {
                    'path': filepaths[2],
                    'extension': extensions[2]
                }
            return True
        except Exception as e:
            logging.error(e)
            return False

    def addConcentrationData(self, study: str, filename: str, headers: int, date: str, st_type: str, authors: str,
      vis:str, filetype:str, unit=None, id_prefix=None, user=None, src_type="patient", delimiter="tab", transpose=False) -> ResultObject:
        """Add data from a concentration file to the database.
        If the according records already exist, it will update them, otherwise they are created.
        FORMAT: Left column metabolites, other columns (can have headers) concentration values

        :param study: The name of the study to add the data to
        :param filename: The filename of the uploaded spectre file to be processed
        :param headers: Row index of the header row (if one exists) for pandas
        :param date: Date or timeframe associated with the file
        :param st_type: Type of the study the file is associated with (serum, urine..)
        :param authors: The author(s) of the study, comma separated
        :param vis: Visibility for the study (public/private)
        :param unit: Unit the metabolites are measured in (defaults to GCKD baseline unit, mmol/L)
        :param id_prefix: Prefix for the IDs
        :param src_type: Source of the data (defaults to 'patient')
        :param transpose: Dictates whether to transpose the pandas df or not
        """
        if not os.path.isfile(filename):
            return ResultObject(msg="This file does not exist!", status=404)
        logging.info("Starting conc.file processing")
        if not unit: unit = "mmol/L"
        if not id_prefix: id_prefix = ""

        # Process the file
        if filetype == 'xlsx':
            processed_file = process_xlsx_concentration_file(filename, headers, transpose=transpose)
        elif filetype == 'csv':
            processed_file = process_csv_concentration_file(filename, headers, delimiter, transpose=transpose)
        elif filetype == 'tsv':
            processed_file = process_csv_concentration_file(filename, headers, "tab", transpose=transpose)
        else:
            return ResultObject(msg="This filetype is not supported!", status=400)
        logging.info("File processing successful")

        # Collect study data:
        study_dict = {
            "study_id": study,
            "study_type": st_type if st_type is not None else "undefined",
            "study_date": date if date is not None else "undefined",
            "study_authors": authors if authors is not None else "unknown",
            "study_files": [os.path.basename(os.path.normpath(filename))],
            "visibility": vis,
            "uploader": user.username if user else "unknown"
        }
        logging.info(f"Study: {study}")

        # Update or create study:
        if self.recordExists(self.study_index, study):
            study_existing = self.getStudyInternal(study).getBody()
            if study_existing["study_files"] is not None and study_dict["study_files"][0] not in study_existing["study_files"]:
                study_dict["study_files"] = study_dict["study_files"] + study_existing["study_files"]
                self.client.update(index=self.study_index, id=study, body={"doc": study_dict})
        else:
            self.createStudy(study_dict)
          
        try:
            if user and user.username != "admin": self.addUserPermission(user.username, study, "manage")
            # Iterate over each sample (== each row in df)
            for metabolite in processed_file.keys():
                logging.info(metabolite)
                if '$' in metabolite: continue #This is for metabolite-to-metabolite normalizations. Ignore for now!

                concentration_values = processed_file.get(metabolite) # Taken from file

                # Get "correct" name for metabolite (check known synonyms)
                metabolite_n = metabolite.lower().replace(" ", "_")
                if metabolite_n in KNOWN_SYNONYMS.keys():
                    metabolite_n = KNOWN_SYNONYMS.get(metabolite.lower().replace(" ", "_"))

                # Add metabolite entry to DB if it doesn't yet exist: 
                if not self.recordExists(self.conc_index, metabolite_n):
                    self.addMetabolite(metabolite_n, "unknown", "unknown", study, unit)
                    current_met_max = 0.0
                elif self.recordExists(self.conc_index, metabolite_n):
                    self.addStudyToMetabolite(metabolite_n, study)
                    current_met_max = float(self.getMetaboliteInternal(metabolite_n).getBody()['metabolite_max'])

                # Collect concentration values for all sources
                # Also compute the average as well as max/min values and the coverage percentage
                nas = 0
                max_value = 0.0
                min_value = 999999.0
                sum_of_values = 0.0
                conc_dict = {"concentration": {}}

                for source in concentration_values.keys():
                    value_as_string = str(concentration_values.get(source)).lower().strip()
                    if value_as_string in ['', 'x', 'na', 'nan', '-']:
                        nas = nas + 1
                        continue
                    if ("x" in value_as_string or "?" in value_as_string):
                        value_as_string = value_as_string.replace("x", "").replace("?", "").strip()
                    float_value = float(value_as_string)
                    src_id = id_prefix + str(source)
                    conc_dict["concentration"][src_id] = float_value
                    if not self.recordExists(self.source_index, src_id):
                        self.createSource({
                            'source_id': src_id,
                            'source_type': src_type,
                            'associated_studies': [study]
                        })
                    else:
                        current_assocs = self.getSource(src_id).getBody()['associated_studies']
                        if study not in current_assocs:
                            current_assocs.append(study)
                            self.client.update(index=self.source_index, id=src_id, body={"doc": {'associated_studies': current_assocs}})
                    sum_of_values = sum_of_values + float_value
                    if float_value > max_value: max_value = float_value
                    if float_value < min_value: min_value = float_value
                conc_dict['pc_relation'] = {'name': "source", "parent": metabolite_n}
                conc_dict['entry_type'] = "source"
                conc_dict['metabolite'] = metabolite_n
                conc_dict['study'] = str(study)
                conc_dict['id'] = str(study+"_"+metabolite_n)
                conc_dict['unit'] = unit
                conc_dict['min_value'] = min_value
                conc_dict['max_value'] = max_value
                conc_dict['average'] = round(sum_of_values / len(concentration_values.keys()), 3)
                conc_dict['coverage'] = round(1 - (nas / len(concentration_values.keys())), 2)
                self.client.index(index=self.conc_index, id=conc_dict['id'], body=conc_dict,routing=metabolite_n)
                if max_value > current_met_max:
                    self.updateMetabolite(metabolite_n, {'metabolite_max': max_value, 'unit': unit})
            return ResultObject(msg=f"Successfully processed the given file ({filename}).")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while parsing XLSX data: {e}", status=500)

    def parseRawSpectrum_Bruker(self, source: str, pattern: str) -> ResultObject:
        """
        Parses a raw spectrum folder in the Bruker format (using R package 'mrbin')

        :param source: ID of the source associated with the spectrum
        :param pattern: File pattern to prefix ID for the raw spectra. Defaults to AKI spectra (AKI_1_24_[ID]). Everything afterwards is ignored.
        :returns Two vectors (in case of success) - one for ppm scale, one for magnitude. This uses the dedicated R microservice for spectra parsing.
        """

        def shrinkArray(array: np.ndarray, binsize: int, cap: int):
            """
            Helper method that bins the spectrum and caps peaks to a certain value (for viewing purposes).

            :param array: The raw spectrum array, containing two dimensions (chemical shift values and peak intensities)
            :param binsize: Number of entries to summarize in one bin
            :param cap: Cutoff value for peaks
            """
            new_array = np.array([])
            for i in range(0, np.int(np.ceil(array.size/binsize))):
                new_array = np.append(new_array, min(np.average(array[i*binsize : i*binsize+binsize+1]), cap))
            return new_array
        
        try:
                current_spectrum = requests.get((self.spectra_service_path + ":" + str(self.spectra_service_port) + "/NMR/v1/parseSpectrum_Bruker"),
                 params={'id': str(source), 'pattern': str(pattern)}).json()

                ppm_array = np.asarray([float(re.sub("\[|\]", "", x)) for x in current_spectrum[0].split(",")])
                mag_array = np.asarray([float(re.sub("\[|\]", "", x)) for x in current_spectrum[1].split(",")])

                ppm_array = shrinkArray(array=ppm_array, binsize=100)
                mag_array = shrinkArray(array=mag_array, binsize=100)

                if ppm_array.size != mag_array.size:
                    return ResultObject(msg="Arrays for ppm values and magnitude differ in size. Aborting.", status=500)
                else:
                    object_list = []
                    stacked_array = np.stack((ppm_array, mag_array), axis=1)
                    for element in reversed(stacked_array.tolist()):
                        object_list.append({'ppm': float(element[0]), 'mag': element[1]})
                    return ResultObject(body=object_list)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while querying raw spectra: {e}", status=500)

    def addMetabolitesFromBiocratesFile(self, filename: str) -> ResultObject:
        """
        Adds metabolites and relevant categories from a tab-separated .csv or.tsv file containing Biocrates data.
        Also imports MetaboINDICATOR data giving some measurements for metabolite ratios.

        :param filename: The name of the file (file is assumed to be in uploads folder)
        """
        try:
            if not os.path.isfile(filename):
                return ResultObject(msg="This file does not exist!", status=404)
            mode="none"
            current_supercategory="none"
            current_category="none"
            with open(filename, 'r') as input:
                for line in input:
                    if line[0] == "#":
                        # Switch (super)categories etc. for categorization
                        commandline_split = line.strip().split("\t")
                        if commandline_split[0].lower() == "#category": current_category = commandline_split[1]
                        elif commandline_split[0].lower() == "#supercategory": current_supercategory = commandline_split[1]
                        elif commandline_split[0].lower() == "#metabolitedata": mode = "metabolitedata"
                        else: mode = "metaboindicator"
                    else:
                        # Adding metabolites:
                        if mode == "metabolitedata":
                            dataline_split = line.strip('\n').split("\t")
                            id = dataline_split[1].lower().replace(' ', '_') if (dataline_split[1] and dataline_split[1] != '') else dataline_split[0].lower().replace(' ', '_')
                            if id in KNOWN_SYNONYMS.keys():
                                id = KNOWN_SYNONYMS[id]
                            additional_info = defaultdict(dict)
                            if dataline_split[2] and dataline_split[2] != '': additional_info['alternate_name'] = dataline_split[2]
                            if dataline_split[3] and dataline_split[3] != '': additional_info['alternate_short'] = dataline_split[3]
                            if dataline_split[4] and dataline_split[4] != '': additional_info['comment'] = dataline_split[4]
                            additional_info['short'] = dataline_split[0]
                            if not self.recordExists(self.conc_index, id):
                                self.addMetabolite(metabolite=id, meta_from=current_supercategory,
                                 meta_cat=current_category, study="None", unit="unknown", additional_info=additional_info)
                            else:
                                self.updateMetabolite(metabolite=id, data=additional_info)
                        # Metaboindicator (not for now..)
                        else:
                            pass
            return ResultObject(msg="Successfully parsed Biocrates file!")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while parsing biocrates data: {e}", status=500)

    def extractNormalConcs(self, filename: str, format: str, specimen: list) -> ResultObject:
        try:
            if not specimen: specimen = self.biospecimen
            logging.info(os.path.join(self.nc_folder, 'normal_concentration', filename))
            if not os.path.isfile(os.path.join(self.nc_folder, 'normal_concentration', filename)):
                return ResultObject(msg=f"The file {filename} was not found!", status=404)
            if format == 'hmdb':
                hmdb_dict = extractNormalConcentrations_HMDB(self.nc_folder, filename, specimen)
                for metabolite in hmdb_dict.keys():
                    already_countered = {}
                    for biospecimen in hmdb_dict[metabolite].keys():
                        if biospecimen == "synonym": continue
                        already_countered[biospecimen] = []
                        spec_counter = 0
                        for concentration_object in hmdb_dict[metabolite][biospecimen]:
                            new_nconc = copy.deepcopy(concentration_object)
                            new_nconc['metabolite'] = metabolite
                            new_nconc['specimen'] = biospecimen
                            new_nconc['source'] = 'hmdb'
                            nconc_id = "hmdb" + "_" + metabolite + "_" + biospecimen + "_from" + filename.split("_")[0] + "_" + str(spec_counter)
                            new_nconc['id'] = nconc_id
                            if not self.recordExists(self.nconc_index, nconc_id):
                                if new_nconc['sex']+"_"+new_nconc['age']+"_"+str(new_nconc['min'])+"-"+str(new_nconc['max']) not in already_countered[biospecimen]:
                                    self.client.index(index=self.nconc_index, id=nconc_id, body=new_nconc)
                                    already_countered[biospecimen].append(new_nconc['sex']+"_"+new_nconc['age']+"_"+str(new_nconc['min'])+"-"+str(new_nconc['max']))
                            else:
                                self.client.update(index=self.nconc_index, id=nconc_id, body={"doc": new_nconc})
                            spec_counter = spec_counter + 1
                logging.info("Done!")
                return ResultObject(msg=f"Successfully extracted normal concentrations from HMDB file {filename}.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while parsing normal concentration file {filename}: {e}", status=500) 

    def addNominalPhenotypeData(self, study: str, filename: str, phenotype: str, headers: int,
     id_prefix=None, id_pattern=None, delimiter="tab", user=None) -> ResultObject:
        """
        Parses a file containing classifier-like phenotype data (nominal values), to add to a specific study.
        This generates [study]_[phenotype] entities detailing the phenotype for each source.
        Only 'id' and 'phenotype' columns in the file are mandatory, the rest is saved in a metadata object.

        :param study: Study to add data for
        :param filename: Name of the file to parse
        :param phenotype: Phenotype identifier (e.g. 'diabetes' or 'aki')
        :param headers: Dictates which row is used as header row
        :param id_prefix: Prefix to prepend to any identifier parsed in the 'id' column
        :param id_pattern: Pattern that is applied to 'id' column when creating the DB object
            Should be a dict with a 'separator' (e.g. '_') and 'keep' specifying which parts of an ID string to keep (e.g. [0,3])
            The full, raw ID is still kept in metadata. We only normalize the ID to get a simpler mapping between indices
        :param delimiter: Delimiter for the file, possible choices: tab, comma, semicolon
        :param user: User performing the request
        """
        try:
            if not study or not self.recordExists(self.study_index, study):
                return ResultObject(msg="Study not provided, or provided study does not exist!", status=400)
            if not self.checkUserPermission(user, study, type="manage"):
                return ResultObject(msg="You don't have the necessary permissions!", status=401)
            if not os.path.isfile(filename):
                return ResultObject(msg="This file does not exist!", status=404)

            # Process the file
            processed_file = process_csv_phenotype_file(filename, headers, delimiter)

            # Create parent object for the study + specific phenotype (has to be created first for routing):
            parsed_phenotype = phenotype.lower().replace(' ', '_')
            pheno_dict = {
                'id': study + "_" + parsed_phenotype,
                'study': study,
                'entry_type': "data",
                'phenotype_id': parsed_phenotype,
                'phenotype_raw': phenotype,
                'pc_relation': {
                    "name": "phenotype_study",
                },
            }

            if self.recordExists(self.phenotype_index, pheno_dict['id']):
                self.client.update(index=self.phenotype_index, id=pheno_dict['id'], body={"doc": pheno_dict})
            else:
                self.client.index(index=self.phenotype_index, id=pheno_dict['id'], body=pheno_dict)

            if id_pattern:
                idk_split = id_pattern.split(" ")
                id_separator = idk_split[0]
                id_keep = idk_split[1]
                id_keep_list = id_keep.split(',')

            # Start parsing file:
            pheno_dict['data'] = {}
            pheno_dict['levels'] = []
            for obj_index in processed_file:
                if processed_file[obj_index]['id'] == "" or processed_file[obj_index]['phenotype'] == "":
                    continue
                parsed_id = processed_file[obj_index]['id']
                
                # For each row: normalize ID
                if id_pattern:
                    parsed_id_split = parsed_id.split(id_separator)
                    parsed_id = id_separator.join([parsed_id_split[int(i)].lstrip('0') for i in id_keep_list])
                if id_prefix:
                    parsed_id = id_prefix + parsed_id
                
                # Add actual phenotype to main phenotype dictionary for the study
                pheno_dict['data'][parsed_id] = processed_file[obj_index]['phenotype']
                if processed_file[obj_index]['phenotype'] not in pheno_dict['levels']:
                    pheno_dict['levels'].append(processed_file[obj_index]['phenotype'])


                # Create object for metadata
                meta_dict = {
                    'associated_source_raw': processed_file[obj_index]['id'],
                    'associated_source': parsed_id,
                    'pc_relation': {'name': "source", "parent": pheno_dict['id']},
                    'entry_type': "metadata",
                    'data': {},
                }
                for key in processed_file[obj_index].keys():
                    if key not in ['phenotype', 'id', 'associated_source', 'pc_relation', 'entry_type']:
                        meta_dict['data'][key] = processed_file[obj_index][key]
                self.client.index(index=self.phenotype_index, id=parsed_id + "_[" + phenotype + "]_" + study, body=meta_dict, routing=pheno_dict['id'])

            # Finally, update master entity
            self.client.update(index=self.phenotype_index, id=pheno_dict['id'], body={"doc": pheno_dict})
            return ResultObject(msg=f"Successfully added phenotype data for study {study} to the database.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Server error during phenotype file parsing: {e}", status=500)

    ################### Data queries ##################

    def filterByMetabolites(self, study_unchecked: list, metabolite_ranges: dict, mode: str, user=None, auth_token=None, phenotypes=None) -> ResultObject:
        """
        Filters the contained concentration data by a set of user-controlled constraints.
        Users can choose to accept any value of a metabolite, set a min/max range or specify that it has to be outside of a set range.

        In "full" mode, all metabolites from a study are added to the results if the constraints are met.
        In "partial" mode, only metabolites that are explicitly given in the query ranges are added.

        :param study_unchecked: List of studies to consider for the query. Unchecked means that permissions will be checked before the query.
        :param metabolite_ranges: Dict containing query parameters for each metabolite
        :param mode: See above, can be "full" or "partial"
        :param user: The user performing the request (required for permission considerations)
        :param auth_token: Auth. token that can be used separately from JWT authentication
        :param phenotypes: List of phenotypes that will be queried in addition to concentration values
        :returns Data subset
        """
        try:
            result_dict = defaultdict(dict)

            # Remove studies the user has no permission for
            study = []
            for st in study_unchecked:
                if self.checkUserPermission(user, st) or self.checkTokenPermission(auth_token,st):
                    study.append(st)

                    result_dict[st] = {}
            # Find all concentration collections pertaining to one of the studies
            if mode == 'partial': # Only return conc. entries that are specified in metabolite_ranges
                metabolite_query = {"query":{"bool":{"must":[
                    {"has_parent":{"parent_type": "metabolite","query":{"terms":{"id": list(metabolite_ranges.keys())}}}},
                    {"terms":{"study": study}}
                ]}}}
            else: # Return all entries
                metabolite_query = {"query":{"bool":{"must":[
                    {"has_parent":{"parent_type": "metabolite", "query": {'match_all': {}}}},
                    {"terms":{"study": study}}
                ]}}}
            
            # Sort queries into specific and unspecific queries
            # Specific queries (minmax, oob) have a constraint attached to them and must be processed first
            # Unspecific queries are considered afterwards
            concentration_collections = scan(self.client, index=self.conc_index, query=metabolite_query)
            specific_query = []
            waiting_list = []
            unfit_src = []
            for collection in concentration_collections:
                collection_mt = collection['_source']['metabolite']
                if collection_mt in metabolite_ranges.keys():
                    collection_range = metabolite_ranges.get(collection_mt)
                    if collection_range['query_type'] != 'any':
                        specific_query.append({'metabolite': collection_mt, 'query_type': collection_range['query_type'],
                         'range': collection_range, 'conc': collection['_source']['concentration'], 'study': collection['_source']['study']})
                    else:
                        waiting_list.append({'metabolite': collection_mt, 'conc': collection['_source']['concentration'], 'study': collection['_source']['study']})
                else:
                    waiting_list.append({'metabolite': collection_mt, 'conc': collection['_source']['concentration'], 'study': collection['_source']['study']})

            # Process specific queries
            for collection_object in specific_query:
                for src, value in collection_object['conc'].items():
                    if src in unfit_src: continue
                    value = float(value)
                    apply_item = False
                    study_descriptor = collection_object['study']
                    if collection_object['query_type'] == 'min' and value >= collection_object['range']['min']: apply_item=True
                    elif collection_object['query_type'] == 'minmax' and value >= collection_object['range']['min'] and value <= collection_object['range']['max']: apply_item=True
                    elif collection_object['query_type'] == 'oob' and (value < collection_object['range']['min'] or value > collection_object['range']['max']): apply_item=True

                    if apply_item:
                        if src not in result_dict[study_descriptor].keys(): result_dict[study_descriptor][src] = {}
                        result_dict[study_descriptor][src][collection_object['metabolite']] = {
                            'value': value,
                            'unit': collection['_source']['unit']
                        }
                    else:
                        unfit_src.append(src)
                        if src in result_dict[study_descriptor].keys():
                            del result_dict[study_descriptor][src]
            
            # Process unspecific queries
            for collection_object in waiting_list:
                for src, value in collection_object['conc'].items():
                    if src in unfit_src: continue
                    value = float(value)
                    apply_item = False
                    study_descriptor = collection_object['study']
                    if mode != 'partial' and (src in result_dict[study_descriptor].keys() or not specific_query): apply_item = True
                    elif collection_object['metabolite'] in metabolite_ranges.keys(): apply_item = True
                    if apply_item:
                        if src not in result_dict[study_descriptor].keys(): result_dict[study_descriptor][src] = {}
                        result_dict[study_descriptor][src][collection_object['metabolite']] = {
                            'value': value,
                            'unit': collection['_source']['unit']
                        }

            # If phenotypes were added to query: Get phenotype data from DB
            if phenotypes:
                phenotype_data = self.getPhenotypeData(phenotypes, study).getBody()
                for study_d in phenotype_data:
                    if study_d not in result_dict.keys(): continue
                    for phenotype in phenotype_data[study_d]:
                        pdata = phenotype_data[study_d][phenotype]['data']
                        if not pdata: continue
                        for src in result_dict[study_d].keys():
                            if src in pdata.keys():
                                result_dict[study_d][src][phenotype] = pdata[src]
            return ResultObject(body=result_dict)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while querying concentration data: {e}", status=500)

    ################### Plots and file generation ###################

    def addPlotToCache(self, plot: BytesIO, plot_id: str) -> ResultObject:
        """
        Adds a plot item to the plot cache.

        :param plot: Image containing the plot
        :param plot_id: Identifier for the plot
        """
        try:
            if plot != "nope" and len(self.generated_plots) > 20:
                self.generated_plots.popitem(last=False)
            if plot != "nope":
                self.generated_plots.update({plot_id: plot})
                return ResultObject(msg=f"Plot added to cache. Retrieve it with ID: {plot_id}")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while adding plot to cache: {e}", status=500)

    def generatePlots(self, plot_list: list, plot_id: str, data_to_plot, plot_nconc:str=None, plot_bins:int=None, phenotypes:list=None, histstyle:str="step|fill") -> ResultObject:
        """
        Based on the commands given in the plot list, creates one or more plots which are then cached in the NMR database.
        Plots are stored with a specific ID to ensure that the right plot is returned through a HTTP call (if an ID was provided).

        :param plot_list: List of "commands" stating which plots to generate from the data
            Possible commands (WIP): corrmap, scatter, hist
        :param plot_id: Identifier for the plot(s) created
        :param data_to_plot: The data to plot
        :param plot_nconc: Normal concentration string for the plots
        :param plot_bins: Number of bins to use for histogram plots
        :param phenotypes: To mark phenotype keys (as opposed to metabolite keys)
        :param phenotype_data: Explicitly contains phenotype data used for plotting
        :remarks The plots are saved in the generated_plots class variable.
        """
        try:
            normal_concs={}
            if (plot_nconc and plot_nconc != ""):
                nconc_split = plot_nconc.split(";")

                for entry in nconc_split:
                    metabolite = entry.split("|")[0]
                    element = entry.split("|")[1]
                    element_split = element.split("-")
                    normal_concs[metabolite] = {'min': float(element_split[0]), 'max': float(element_split[1])}

            data_frames, unit_map, phenotype_map = self.plotter.dictToDataFrame_metaboliteQuery(data_to_plot=copy.deepcopy(data_to_plot), phenotypes=phenotypes)
            for plot_command in np.unique(plot_list):
                plot_command = plot_command.strip().lower()
                if plot_command == "scatter":
                    self.addPlotToCache(self.plotter.generateScatterPlot(data_frames, unit_map, phenotype_map, normal_concs, phenotypes=phenotypes), plot_id+"_scatter")
                if plot_command == "corrmap":
                    self.addPlotToCache(self.plotter.generateCorrelationPlot(data_frames), plot_id+"_corrmap")
                if plot_command == "hist":
                    self.addPlotToCache(self.plotter.generateHistPlot(data_frames, unit_map, phenotype_map, normal_concs, plot_bins, phenotypes=phenotypes, histstyle=histstyle), plot_id+"_hist")
            return ResultObject(msg=f"Successfully created plots with ID {plot_id}")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while creating plots: {e}", status=500)
        
    def generateCSV(self, data_to_write: dict, metabolites: list, na="blank", separator="tabulator", filename="unnamed.csv", user=None) -> ResultObject:
        """
        Generates a .csv file from arbitrary input. However, the input must be formatted in a way that resembles service output.
        Also allows choosing an output format and a specific field separator.

        :param data_to_write: Dictionary containing data entries. Must be in MetaboSERV format (Generally given because this is called by frontend)
        :param format: Output format. Currently only supports and defaults to a format resembling GCKD FU2
        :param separator: Separator character (e.g. '\t' or ';')
        :param na: Defines how to handle NAs
        :param metabolites: List of metabolites to put in the first column of the CSV file
        :param filename: Name of the generated file
        :param user: The user performing the request
        :returns 200: Success
        """
        try:
            sep_character = '\t' if separator == 'tab' else ';' if separator == 'scl' else '|'
            na_character = 'x' if na == "rep-x" else '.' if na == 'rep-dot' else 'NA' if na == 'rep-na' else ''
            complete_data = defaultdict(dict)
            for study in data_to_write.keys():
                complete_data = {**complete_data, **data_to_write[study]}
            with open(os.getcwd() + "/" + filename, 'w') as csv_file:
                samples = complete_data.keys()
                csv_file.write(sep_character + sep_character.join(samples) + '\n') # Header
                for mt in metabolites:
                    csv_file.write(capitalizeChemical(mt) + sep_character)
                    for id in samples:
                        entry = complete_data.get(id)
                        if mt not in entry.keys():
                            csv_file.write(na_character + sep_character)
                        else:
                            csv_file.write(str(entry[mt]["value"]) + sep_character)
                    csv_file.write('\n')
            if not user:
                csv_id = str(uuid.uuid1())
            else:
                csv_id = user.username
            self.generated_csv[csv_id] = filename
            return ResultObject(msg=f'Successfully generated a CSV file ({filename}) with the given input data.', body={'id': csv_id})
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while creating a CSV file: {e}", status=500)

if __name__ == "__main__":
    pass # Tests/Asserts could go here
