from collections import defaultdict
import json
import logging
import os
from pyexpat import ExpatError
from typing import OrderedDict
from flask import Response, jsonify
import yaml
import pandas as pd
import numpy as np
from werkzeug.utils import secure_filename
import xmltodict as xd
import re
import magic


# Contains all column keys that are included (capitalization does not matter)
SUPPORTEDKEYS = ['measurementdate', 'measurement_date', 'measurement date', 'samplename',
                 'sample name', 'sample_name', 'sampleid', 'sample id', 'sample_id', 'date', 'name', 'reporting date', 'reportingdate', 'reporting_date', 'instrument', 'probehead', 'experiment']

# Template for a study/an "experiment" containing metabolomics data
# Each separate file (e.g. .csv) should be considered as a single study
# Studies can be "linked" using the associated_studies key
INDEXMAPPING_STUDY = {
    "mappings": {
        "properties": {
            "study_type": {"type": "text"},  # e.g. Urine
            "study_id": {"type": "keyword"},  # e.g. AKI
            "study_authors": {"type": "text"},  # Study authors
            "study_date": {"type": "date"},  # Publication date for the stuy
            "study_files": {"type": "text"},  # Files associated with study
            "associated_studies": {"type": "text"},  # Link a study to another
            "visibility": {"type": "text"} # Public or private
        }
    }
}

# Template for a specific source for NMR data (e.g. a specific patient)
# Each source should be mapped to exactly one ID that is consistent through studies
# Data field is left vague on purpose as of now to accomodate format changes
INDEXMAPPING_SOURCE = {
    "mappings": {
        "properties": {
            "source_type": {"type": "text"},
            "source_id": {"type": "keyword"},
            "associated_studies": {"type": "keyword"},
        }
    }
}

# Defines a basic template mapping for normal concentration data.
INDEXMAPPING_DATA = {
    "mappings": {
        "properties": {
            "metabolite": {"type": "keyword"},
            "specimen": {"type": "text"},
            "age": {"type": "text"},
            "unit": {"type": "text"},
            "sex": {"type": "text"},
            "comment": {"type": "text"},
            "condition": {"type": "text"},
            "min": {"type": "double"},
            "max": {"type": "double"},
            "source": {"type": "text"},
        }
    }
}

# Defines a basic user account mapping to keep in ElasticSearch.
# Password is only saved in hashed+salted form.
# Salt should be changed each time the password is changed.
INDEXMAPPING_USER = {
    "mappings": {
        "properties": {
            "username": {"type": "keyword"},
            "password": {"type": "text"},
            "role": {"type": "text"}, #admin or user
            "salt": {"type": "text"},
            "permissions": {
                "type": "nested",
                "properties": {
                    "manage": {"type": "keyword"},
                    "view": {"type": "keyword"},
                }
            }
        }
    }
}

# Defines a metabolite + concentration mapping for ElasticSearch.
# Modeled after the "new" approach of keeping concentration data for the metabolites, not the sources.
# The pc_relation field joins one metabolite to a number of concentration entries (1:n)
INDEXMAPPING_MET = {
    "settings": {
        "number_of_shards": 4
    },
    "mappings": {
        "properties": {
            "id": {"type": "keyword"},
            "entry_type": {"type": "text"},
            "pc_relation": {
                "type": "join",
                "eager_global_ordinals": True,
                "relations": {
                    "metabolite": "source"
                }
            },
            "concentration": {
                "type": "nested",
            },
            "study": {"type": "keyword"},
            "studies": {"type": "text"},
        }
    }
}

# Defines a basic framework for phenotype data.
INDEXMAPPING_PHE = {
    "settings": {
        "number_of_shards": 2
    },
    "mappings": {
        "properties": {
            "id": {"type": "keyword"},
            "entry_type": {"type": "text"}, #data or metadata
            "associated_source": {"type": "keyword"},
            "phenotype_id": {"type": "keyword"},
            "phenotype_raw": {"type": "text"},
            "study": {"type": "keyword"},
            "data": {
                "type": "nested",
            },
            "pc_relation": {
                "type": "join",
                "eager_global_ordinals": True,
                "relations": {
                    "phenotype_study": "source"
                }
            },
        }
    }
}

# Maps metabolite descriptors to a more common name
KNOWN_SYNONYMS = {
    "pyruvic_acid": "pyruvate",
    "3-hydroxybutyrate": "3-hydroxybutyric_acid",
    "trimethylamine_n-oxide": "trimethylamine-n-oxide",
    "alanine_(1.46-1.44)": "alanine",
    "glucose_(3.45)": "glucose",
}

# For normalization of the different measurement categories
CONCENTRATION = ['conc', 'conc.', 'concentration']
RAWCONCENTRATION = ['rawconc', 'rawconc.',
                    'raw_concentration', 'raw_conc', 'raw_conc.']
ERRORCONCENTRATION = ['errconc', 'errconc.',
                      'error_concentration', 'err_conc', 'err_conc.']
SIGMACORR = ['sigcorr', 'sig_corr', 'sigcorr.']
LOD = ['lod', 'lod.', 'limit of detection']
VMIN = ['vmin', 'v_min', 'vmin.']

# For column normalization
EXPID_LIPO = ['name']
EXPID_METABO = ['sampleid', 'sample_id']
SOURCEID = ['sample name', 'sample_name', 'samplename']
NORMALIZE = EXPID_LIPO + SOURCEID + EXPID_METABO

# Maps file extensions to their acceptable mime types / media types
# This is used for file type detection using python-magic (libmagic)
EXT_MIMETYPE_MAP = {
    '.csv': ["text/csv", "text/plain"],
    '.tsv': ["text/csv", "text/plain", "text/tab-separated-values"],
    '.xlsx': ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],
    '.yml': ["text/plain", "application/yaml", "text/yaml"],
}

class ResultObject(object):

    msg: str # Message describing the body (if there is any) or the process
    body: object # Can be anything, but should be convertable to JSON
    status: int # HTTP Status code to further describe the result (e.g. 200)

    def __init__(self, msg="Success", body=None, status=200):
        """
        Creates a new ResultObject. This happens whenever a function returns some output.
        """
        setattr(self, 'msg', msg)
        setattr(self, 'body', body)
        setattr(self, 'status', status)

    def __getitem__(self, item: str):
        return getattr(self, str(item))

    def getBody(self):
        # This is really just for code clarity. Calling getBody() or .body is equivalent.
        # Returns body as a dict
        return getattr(self, 'body')

def set_delimiter(del_param: str) -> str:
    """
    Returns a file delimiter char based on a string passed through a URL.

    :param del_param: The string in question
    :returns According delimiter for the del_param string
    """
    if not del_param or del_param == 'tab':
        return '\t'
    elif del_param == 'comma':
        return ','
    elif del_param == 'semicolon':
        return ';'
    return '\t'

def read_config(yml_path: str) -> dict:
    """
    Reads a yml config file. For details refer to the readme.

    :param yml_path: The path to the yml file (default is metaboserv_conf.yml)
    :returns A dictionary containing configuration details
    """
    with open(yml_path, 'r') as stream:
        config_data = yaml.safe_load(stream)
        return config_data

def process_csv_concentration_file(filename: str, headers: int, delimiter: str, transpose=False) -> dict:
    """
    Reads a .csv file containing concentration data and returns it as a dictionary.

    :param filename: Path to the file (by default ./uploads/file)
    :param headers: Marks the header line for pandas
    :param delimiter: The delimiter to use for the file
    :param transpose: Dictates whether to transpose the pandas dataframe or not
    :returns Dictionary containing data from a concentration file
    """
    with open(filename, 'r') as concentration_file:
        raw_processed_file = pd.read_csv(
            concentration_file, header=headers, skip_blank_lines=True, sep=set_delimiter(delimiter))
        if transpose:
            raw_processed_file = raw_processed_file.transpose()
        raw_processed_file = raw_processed_file.replace(
            [np.inf, -np.inf], -1.0)
    return raw_processed_file.to_dict('index')

def process_csv_phenotype_file(filename: str, headers: int, delimiter: str) -> dict:
    """
    Reads a .csv file containing concentration data and returns it as a dictionary.

    :param filename: Path to the file (by default ./uploads/file)
    :param headers: Marks the header line for pandas
    :param delimiter: The delimiter to use for the file
    :returns Dictionary containing data from a concentration file
    """
    with open(filename, 'r') as pheno_file:
        raw_processed_file = pd.read_csv(
            pheno_file, header=headers, skip_blank_lines=True, sep=set_delimiter(delimiter))
    return raw_processed_file.to_dict('index')

def process_xlsx_concentration_file(filename: str, headers: int, transpose=False) -> dict:
    """
    Reads a .xlsx file containing concentration data and returns it as a dictionary.

    :param filename: Path to the file (by default ./uploads/file)
    :param headers: Marks the header line for pandas
    :param transpose: Dictates whether to transpose the pandas dataframe or not
    :returns Dictionary containing data from a concentration file
    """
    # Has to be opened in binary mode (rb)
    try:
        with open(filename, 'rb') as concentration_file:
            raw_processed_file = pd.read_excel(concentration_file, index_col=0, usecols=lambda x: x != 0, header=headers)
            if transpose:
                raw_processed_file = raw_processed_file.transpose()
            raw_processed_file = raw_processed_file.replace(
                [np.inf, -np.inf], -1.0)
        return raw_processed_file.to_dict('index')
    except Exception as e:
        logging.error(e)
    
def normalize_columns(key: str) -> dict:
    """
    Return a set key classifier in order to normalize slight variations across different files.

    :param key: The column key
    :returns Normalized key
    """
    if key.lower() in EXPID_LIPO:
        return "experiment_id_lipo"
    elif key.lower() in SOURCEID:
        return "source_id"
    elif key.lower() in EXPID_METABO:
        return "experiment_id_metabo"
    return key

def normalize_categories(category: str) -> str:
    """
    Return a set category classifier in order to normalize slight variations across different files.

    :param category: The classifier
    :returns Normalized classifier
    """
    if category.lower() in CONCENTRATION:
        return 'concentration'
    elif category.lower() in RAWCONCENTRATION:
        return "raw_concentration"
    elif category.lower() in ERRORCONCENTRATION:
        return "error_concentration"
    elif category.lower() in SIGMACORR:
        return "sig_corr"
    elif category.lower() in LOD:
        return "lod"
    elif category.lower() in VMIN:
        return "v_min"
    else:
        return "v_max"

def createResponse(payload_data: ResultObject) -> Response:
    """
    Creates a Flask Response object containing the payload of a function.

    :param payload_data: The tuple in question (data, status, content type)
    :returns Response object
    """
    return Response(json.dumps({'body': payload_data.body, 'msg': str(payload_data.msg)}), status=int(payload_data.status), content_type='application/json')

def createErrorResponse(mandatory_parameters: list) -> Response:
    """
    Creates a Flask Error Response object.
    Mandatory parameters are shown in the error message to simplify error handling for the API consumer.

    :param mandatory_parameters: Parameters of the function that are required
    :returns Error Response object
    """
    if not mandatory_parameters:
        param_string = "None"
    elif len(mandatory_parameters) == 1:
        param_string = mandatory_parameters[0]
    else:
        param_string = ", ".join(mandatory_parameters)
    return jsonify(msg=f"HTTP Query failed. Are all required parameters provided? ({param_string})"), 500

def createMetaboliteQuery(metabolites: list) -> dict:
    """
    Creates the metabolite range query dict that is required to search for specific
    metabolites by range in ElasticSearch.

    :param metabolites: List of metabolites (and their ranges)
    :returns dict with the query, list of normalized metabolite names
    """
    try:
        metabolite_dict = {}
        metabolite_names = []
        for metabolite in metabolites:
            metabolite_split = metabolite.split(":")
            metabolite_normalized = metabolite_split[0].lower().replace(' ', '_')
            if metabolite_normalized in KNOWN_SYNONYMS.keys():
                            metabolite_normalized = KNOWN_SYNONYMS[metabolite_normalized]
            metabolite_names.append(metabolite_normalized)
            if len(metabolite_split) == 1:
                metabolite_dict[f'concentration.{metabolite_normalized}.value'] = {
                    "gt": 0.0
                }
            elif len(metabolite_split) == 2:
                if ("oob" in metabolite_split[1]):
                    oob_split = metabolite_split[1].split("|")
                    range_split = oob_split[1].split("-")
                    metabolite_dict[f'concentration.{metabolite_normalized}.value'] = {
                        "bool": {
                            "should": [
                                {"range": {f'concentration.{metabolite_normalized}.value': {"lt": float(range_split[0])}}},
                                {"range": {f'concentration.{metabolite_normalized}.value': {"gt": float(range_split[1])}}}
                            ]
                        }
                    }
                else:
                    range_split = metabolite_split[1].split("-")
                    if len(range_split) == 1:
                        metabolite_dict[f'concentration.{metabolite_normalized}.value'] = {
                        "gt": float(range_split[0])
                        }
                    elif len(range_split) == 2:
                        metabolite_dict[f'concentration.{metabolite_normalized}.value'] = {
                        "gte": float(range_split[0]),
                        "lte": float(range_split[1])
                        }
        return metabolite_dict, metabolite_names
    except Exception as e:
        logging.error(e)
        return None, None

def createMetaboliteQueryV2(metabolites: list) -> dict:
    """
    Creates the metabolite range query dict that is required to search for specific
    metabolites by range in the concentration data, directly in python.

    :param metabolites: List of metabolites (and their ranges)
    :returns dict with the query, list of normalized metabolite names
    """
    try:
        metabolite_dict = {}
        for metabolite in metabolites:
            metabolite_split = metabolite.split(":")
            metabolite_normalized = metabolite_split[0].lower().replace(' ', '_')
            if metabolite_normalized in KNOWN_SYNONYMS.keys():
                            metabolite_normalized = KNOWN_SYNONYMS[metabolite_normalized]
            if len(metabolite_split) == 1:
                metabolite_dict[metabolite_normalized] = {
                    "min": None,
                    "max": None,
                    "type": "default",
                }
            elif len(metabolite_split) == 2:
                if ("oob" in metabolite_split[1]):
                    oob_split = metabolite_split[1].split("|")
                    range_split = oob_split[1].split("-")
                    metabolite_dict[metabolite_normalized] = {
                        "min": float(range_split[0]),
                        "max": float(range_split[1]),
                        "type": "oob",
                    }
                else:
                    range_split = metabolite_split[1].split("-")
                    if len(range_split) == 1:
                        metabolite_dict[metabolite_normalized] = {
                        "min": float(range_split[0]),
                        "max": None,
                        "type": "default",
                    }
                    elif len(range_split) == 2:
                        metabolite_dict[metabolite_normalized] = {
                        "min": float(range_split[0]),
                        "max": float(range_split[1]),
                        "type": "default",
                    }
        return metabolite_dict
    except Exception as e:
        logging.error(e)
        return None

def createDiffQuery(metabolites: list, metabolite_list: dict) -> dict:
    """
    Creates the metabolite range query dict that is required to search for specific
    metabolites by difference between studies in ElasticSearch.

    :param metabolites: List of metabolites (and their minimum difference)
    :param metabolite_list: List of current studies in ES
    :returns Dict with the query
    """
    try:
        metabolite_dict = {}
        current_metabolites = metabolite_list.keys()
        for metabolite in metabolites:
            metabolite_split = metabolite.split(":")
            metabolite_normalized = metabolite_split[0].lower().replace(' ', '_')
            if metabolite_normalized in KNOWN_SYNONYMS.keys():
                            metabolite_normalized = KNOWN_SYNONYMS[metabolite_normalized]
            if metabolite_normalized not in current_metabolites:
                continue
            if len(metabolite_split) == 1:
                metabolite_dict[metabolite_normalized] = "any"
            else:
                metabolite_dict[metabolite_normalized] = str(metabolite_split[1])
        return metabolite_dict
    except Exception as e:
        logging.error(e)
        return None

def validate_cf(filepath, extension) -> bool:
    """
    Validates concentration file format.
    The first line should contain the following header designations:
    concentration ([unit]), 
    """
    pass

def validate_mf(filepath, extension) -> bool:
    """
    Validates metadata file format.
    Can have arbitrary labels on either rows or columns (csv/tsv/xlsx) or arbitrary key-value pairs (yml).
    However, a number of labels are pre-defined and shown in the frontend (e.g. study_type, study_date)
    Important is that its either 2 rows or 2 columns for the csv case.
    """
    pass

def validate_pf(filepath, extension) -> bool:
    """
    Validates phenotype data file.
    There are only two mandatory columns/rows here - "id" and at least one "phenotype_[...]" column.
    Phenotype columns should be marked with phenotype_[name], everything else will be regarded as phenotypical metadata.
    """
    pass

def handleFileUpload(study: str, uploaded_files: list, upload_folder: str, extensions: list) -> ResultObject:
    """
    Handles file upload, triggered through a POST request to the according Flask endpoint.
    Files are validated using python-magic, also the format is checked.
    If validation fails, files are deleted again - otherwise they are kept and are then ready to be processed.

    :param uploaded_files: List of files in request: Conc. file (mandatory), metadata file, phenotype data file (both optional).
    :param upload_folder: Designated folder to store uploads
    :param extensions: Allowed file extensions 
    """
    try:
        if not study or not uploaded_files:
            return ResultObject(msg="Study or file list missing!", status=400)
        if not uploaded_files[0]:
            return ResultObject(msg="Concentration file missing!", status=400)

        # General validation based on extension and filetype (using magic)
        filepaths = []
        recorded_extensions = []
        for file in uploaded_files:
            if file and file.filename != '':

                # Validate extension and save file
                file_ext = os.path.splitext(file.filename)[1]
                if file_ext not in extensions:
                    return ResultObject(msg=f'File extension was not recognized ({file_ext}). Please amend your file(s).', status=403)
                recorded_extensions.append(file_ext)
                new_filepath = os.path.join(
                    upload_folder, study, secure_filename(file.filename))
                if not os.path.exists(new_filepath):
                    file.save(new_filepath)
                    file.close()
                else:
                    os.replace(src=new_filepath, dst=new_filepath+"_old")
                    file.save(new_filepath)

                # Validate saved file with magic
                mimetype_f = magic.from_file(new_filepath, mime=True)
                mimetype_b = magic.from_buffer(open(new_filepath, "rb").read(2048), mime=True)
                if mimetype_f != mimetype_b:
                    return ResultObject(msg="Mimetype mismatch detected by libmagic", status=403)
                if mimetype_f not in EXT_MIMETYPE_MAP[file_ext]:
                    return ResultObject(msg="Unaccepted mimetype detected by libmagic", status=403)
                
                filepaths.append(new_filepath)
            else:
                filepaths.append(None)
                recorded_extensions.append(None)

        # Format validation
        try:
            validated = True
            validated = validate_cf(filepaths[0], recorded_extensions[0])
            if not validated:
                return ResultObject(msg="Concentration file format could not be validated.", status=406)
            if filepaths[1]:
                validated = validate_mf(filepaths[1], recorded_extensions[1])
                if not validated:
                    return ResultObject(msg="Metadata file format could not be validated.", status=406)
            if filepaths[2]:
                validated = validate_pf(filepaths[2], recorded_extensions[2])
                if not validated:
                    return ResultObject(msg="Phenotype data file format could not be validated.", status=406)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception while validating file format:\n{e}", status=500)
        return ResultObject(msg="Files were successfully validated and saved!", body={'filepaths': filepaths, 'extensions': extensions}, status=200)
    except Exception as e:
        logging.error(e)
        return ResultObject(msg=f"Encountered an exception while handling file upload:\n{e}", status=500)

def handleFileDeletion(filename: str) -> ResultObject:
    """
    Handles file deletion, triggered through a POST request to the according Flask endpoint.

    :param filename: The name of the file to delete
    """
    try:
        if not os.path.exists(filename):
            return ResultObject(msg='File does not exist.', status=404)
        else:
            os.remove(filename)
            return ResultObject(msg='File removed successfully.')
    except Exception as e:
        logging.error(e)
        return ResultObject(msg=f"Encountered an exception during file deletion:\n{e}", status=500)

def capitalizeChemical(chemical: str) -> str:
    """
    Capitalizes the first letter of each word in a chemical name.

    :param chemical: The chemical name in question
    :returns Capitalized version of a chemical name
    """
    chemical = chemical.lower().replace('_', ' ')
    chemical_s = re.split(" ", chemical)
    for i in range(len(chemical_s)):
        chemical_s[i] = chemical_s[i].capitalize()
    chemical_s_new = "_".join(chemical_s)

    chemical_b = re.split("-", chemical_s_new)
    for i in range(len(chemical_b)):
        if chemical_b[i].lower() == "edta":
            chemical_b[i] = "EDTA"
        else:
            chemical_b[i] = chemical_b[i][0].upper() + chemical_b[i][1:]
    chemical_final = "-".join(chemical_b).replace('_', " ")

    return chemical_final

def extractNormalConcentrations_HMDB(file_folder:str, file_name: str, biospecimen: list) -> dict:
    """
    Parses a HMDB Metabolite data file (.xml format) and extracts the normal concentrations.
    Can then be used to create an xxx_parsed file in the data folder (api-side).

    :param file_folder: Folder the xml file is in
    :param file_name: Name of the file (e.g. serum_metabolites.xml)
    :param metabolite_list: List of metabolites to check against
    :param biospecimen: List of biospecimen to keep concentration data for (e.g. blood, serum)
    :returns Dict with data from HMDB metabolite ontology
    """

    def unravelHMDBConc(conc_object: dict):
        """
        Helper method that normalizes the messy normal concentration entries as provided by the HMDB.

        :param conc_object: HMDB normal concentration entry
        :returns Normalized object
        """
        try:
            if 'concentration_value' in conc_object.keys() and conc_object['concentration_value']:
                parsed_min, parsed_max = parseHMDBConcValue(conc_object['concentration_value'])
            else:
                return None
            if parsed_min == -1 or parsed_max in [-1, 0]:
                return None
            unraveled = {
                'min': round(parsed_min,2),
                'max': round(parsed_max,2),
                'age': normalizeHMDBAge(conc_object['subject_age'].lower() if ('subject_age' in conc_object.keys() and conc_object['subject_age']) else "not specified"),
                'condition': conc_object['subject_condition'].lower() if ('subject_condition' in conc_object.keys() and conc_object['subject_condition']) else "not specified",
                'comment': conc_object['comment'] if ('comment' in conc_object.keys() and conc_object['comment']) else "None",
                'unit': conc_object['concentration_units'].lower() if ('concentration_units' in conc_object.keys() and conc_object['concentration_units']) else "not specified",
                'sex': conc_object['subject_sex'].lower() if ('subject_sex' in conc_object.keys() and conc_object['subject_sex']) else "not specified",
            }
            if unraveled['unit'].lower() == 'um':
                unraveled['unit'] = 'mmol/L'
                unraveled['min'] = unraveled['min']/1000
                unraveled['max'] = unraveled['max']/1000
            return unraveled
        except Exception as e:
            logging.error(e)

    def parseHMDBConcValue(value_string: str):
        """
        Parses a normal concentration value string as provided by HMDB (they can be quite "unclean")

        :param value_string: The string to parse
        :returns min and max values for a metabolite
        """
        def isfloat(num):
            """
            Checks if a string can be cast directly to a float.

            :param num: The string in question
            :returns True if floatable, False otherwise
            """
            try:
                float(num)
                return True
            except ValueError:
                return False

        try:
            if not value_string: return -1, -1
            value_string = value_string.replace(",",".")
            if value_string.startswith("-") or value_string.startswith("–"):
                value_string=value_string[1:len(value_string)]
            if "+/-" in value_string:
                values = value_string.split("+/-")
                return max(0, float(values[0].strip()) - float(values[1].strip())), float(values[0].strip()) + float(values[1].strip())
            if "<" in value_string:
                return 0.0, float(value_string.replace("<", "").replace("=",""))
            if ">" in value_string:
                return float(value_string.replace(">", "").replace("=","")), 999999.9
            if " (" in value_string:
                value_split = value_string.split(" (")
                values = re.split('-|–', value_split[1].replace(")", ""))
                return float(values[0]), float(values[1])
            if "(" in value_string and "-" not in value_string and "–" not in value_string:
                values = value_string.split("(")
                return float(values[0]) - float(values[1]), float(values[0]) + float(values[1])
            if "(" in value_string and ("-" in value_string or "–" in value_string):
                value_split = value_string.split("(")
                values = re.split('-|–', value_split[1].replace(")", ""))
                return float(values[0].strip()), float(values[1].strip())
            if "-" in value_string:
                values = value_string.split("-")
                return float(values[0].strip()), float(values[1].strip())
            if "–" in value_string:
                values = value_string.split("–")
                return float(values[0].strip()), float(values[1].strip())
            if isfloat(value_string):
                return float(value_string), float(value_string)
            return -1, -1
        except ValueError:
            return -1, -1
        except Exception as e:
            logging.error(e)
            logging.error(f"Encountered error during conc. value parsing: {e}")
            return -1, -1

    def normalizeHMDBAge(value_string: str):
        """
        Normalizes HMDB age annotations into the following groups:
        Newborn, infant, children, adolescent, adult
        """
        if "newborn" in value_string: return "newborn"
        if "infant" in value_string: return "infant"
        if "child" in value_string: return "children"
        if "adolescent" in value_string: return "adolescents"
        if "adult" in value_string: return "adults"
        return "not specified"
        
    result_dict = defaultdict(dict)
    with open(os.path.join(file_folder, file_name), 'r') as xml_file:
        current_buffer = ""
        while True:
            next_line = xml_file.readline()
            if not next_line:
                break
            if next_line.strip().rstrip('\n') == "<metabolite>":
                current_buffer=next_line
            elif next_line.strip().rstrip('\n') == "</metabolite>":
                current_buffer = current_buffer + next_line  
                try:
                    entry = xd.parse(current_buffer)['metabolite']
                except ExpatError:
                    logging.error("ExpatError while parsing HMDB file.\n")
                if not entry:
                    continue

                synonym_to_use = None
                name_to_use = None
                temp_dict = defaultdict(dict)
                potential_entry = entry['name'].strip().lower().replace(" ", "_")
                if potential_entry in KNOWN_SYNONYMS.keys():
                    name_to_use = KNOWN_SYNONYMS.get(potential_entry)
                    synonym_to_use = potential_entry
                else: name_to_use = potential_entry

                if not entry['normal_concentrations']:
                    continue
                if not entry['normal_concentrations']['concentration']:
                    continue

                if synonym_to_use: temp_dict[name_to_use] = {'synonym': synonym_to_use}
                else: temp_dict[name_to_use] = {}
                
                if type(entry['normal_concentrations']['concentration']) in [dict, OrderedDict]:
                    conc = entry['normal_concentrations']['concentration']
                    if conc['biospecimen'].lower() in biospecimen:
                        if conc['biospecimen'].lower() not in temp_dict[name_to_use].keys():
                            temp_dict[name_to_use][conc['biospecimen'].lower()] = []
                        temp_dict[name_to_use][conc['biospecimen'].lower()].append(unravelHMDBConc(conc))
                elif type(entry['normal_concentrations']['concentration']) is list:
                    for conc_index in range(len(entry['normal_concentrations']['concentration'])):
                        try:
                            conc = entry['normal_concentrations']['concentration'][conc_index]
                            if conc['biospecimen'].lower() in biospecimen and conc['concentration_value'] is not None and conc['concentration_units'] is not None:
                                if conc['biospecimen'].lower() not in temp_dict[name_to_use].keys():
                                    temp_dict[name_to_use][conc['biospecimen'].lower()] = []
                                dict_to_add = unravelHMDBConc({x:conc[x] for x in list(conc.keys())[:-1]})
                                if dict_to_add:
                                    temp_dict[name_to_use][conc['biospecimen'].lower()].append(dict_to_add) 
                        except KeyError:
                            logging.error(f"Failed trying to add concentration data for metabolite {name_to_use}.")
                else:
                    continue
                if len(temp_dict[name_to_use].keys()) != 0:
                    result_dict[name_to_use] = temp_dict[name_to_use]
                current_buffer = ""
            else:
                current_buffer = current_buffer + next_line
    return result_dict

class obj(object):
    def __init__(self, dict_):
        self.__dict__.update(dict_)

def dict2obj(d):
    return json.loads(json.dumps(d), object_hook=obj)

