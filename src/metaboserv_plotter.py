from collections import defaultdict
import logging
from matplotlib.patches import Rectangle
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import seaborn as sns
import numpy as np
import io
from metaboserv_helpers import capitalizeChemical

matplotlib.use('Agg')

class MetaboservPlotter:

    def __init__(self):
        pass

    def dictToDataFrame_metaboliteQuery(self, data_to_plot: dict, phenotypes:list=None) -> pd.DataFrame and dict:
        """
        Turns query results into a pandas dataframe for easier plotting.
        Also returns a map for metabolites/units.

        :param data_to_plot: The data to be plotted in the histogram. Handled by the TS frontend
        :returns pandas DataFrame, map between metabolites and their units, number of studies
        """
        unit_map = defaultdict(dict) # Holds the unit each metabolite was measured in
        phenotype_map = defaultdict(dict)
        
        data_frame_list = []
        
        # Aggregate and reformat concentration data
        keys_to_delete = []
        
        for study in data_to_plot.keys():
            res_dict = defaultdict(dict)
            phenotype_map[study] = {}
            for source_key in data_to_plot[study].keys():
                if source_key not in res_dict.keys():
                    res_dict[source_key] = {}
                for metabolite_key in data_to_plot[study][source_key].keys():
                    if phenotypes and metabolite_key in phenotypes:
                        if metabolite_key not in phenotype_map[study].keys():
                            phenotype_map[study][metabolite_key] = {}
                        # Phenotypes are kept in a different map, key is saved for later deletion
                        # This is done as having phenotypes in there would brick the concentration data plotting
                        phenotype_map[study][metabolite_key][source_key] = data_to_plot[study][source_key][metabolite_key]
                        if metabolite_key not in keys_to_delete:
                            keys_to_delete.append(metabolite_key)
                        continue

                    if 'value' in data_to_plot[study][source_key][metabolite_key].keys():
                        if metabolite_key not in unit_map.keys():
                            unit_map[metabolite_key] = data_to_plot[study][source_key][metabolite_key]['unit']
                        if metabolite_key not in res_dict[source_key].keys():
                            res_dict[source_key][metabolite_key] = {}
                        res_dict[source_key][metabolite_key] = data_to_plot[study][source_key][metabolite_key]['value']
                    else:
                        res_dict[source_key][metabolite_key] = np.nan
                res_dict[source_key]['study'] = study

            # Remove phenotypes from general plotting data:
            # (Has to be done afterwards as you cant modify a dict during iteration)
            for source_key in res_dict.keys():
                if source_key == 'study': continue
                for ktd in keys_to_delete:
                    if ktd in res_dict[source_key].keys():
                        del res_dict[source_key][ktd]

            data_frame = pd.DataFrame.from_dict(res_dict).T
            data_frame_list.append(data_frame)

        return data_frame_list, unit_map, phenotype_map

    def generateScatterPlot(self, data_frames: list, unit_map: dict, phenotype_map:dict, nconc: dict, phenotypes:list = None):
        """
        Generates a scatterplot.

        :param data_to_plot: pandas DataFrame containing the data to plot
        :param nconc: Normal concentration data (min/max) for the metabolites
        :returns BytesIO object containing the plot image
        """
        if phenotypes and phenotype_map and len(phenotype_map) > 0:
            for df in data_frames:
                for study in phenotype_map.keys():
                    for pf in phenotype_map[study].keys():
                        for src in phenotype_map[study][pf].keys():
                            df.loc[src,pf] = phenotype_map[study][pf][src] if phenotypes else "Unknown"

        study_number = len(data_frames)
        data_frames = pd.concat(data_frames, ignore_index=True)
        
        f, ax = plt.subplots(figsize=(11, 9))
        metabolites_to_plot = list(unit_map.keys())
        if len(metabolites_to_plot) < 2:
            return "nope"

        c_palette = "Blues" if study_number == 1 else sns.diverging_palette(250, 10, n=study_number)
        if phenotypes and phenotype_map and len(phenotype_map) > 0:
            sns.scatterplot(data=data_frames, x=metabolites_to_plot[0], y=metabolites_to_plot[1], hue="study",  style=phenotypes[0], palette=c_palette, s=100)
        else:
            sns.scatterplot(data=data_frames, x=metabolites_to_plot[0], y=metabolites_to_plot[1], hue="study", palette=c_palette, s=100)
        
        if nconc and metabolites_to_plot[0] in nconc.keys():
            mx_nconcs = nconc[metabolites_to_plot[0]]
            ax.add_patch(Rectangle((mx_nconcs['min'], ax.get_ylim()[0]), (mx_nconcs['max']-mx_nconcs['min']), ax.get_ylim()[1]+abs(0-ax.get_ylim()[0]), facecolor="lightgreen", fill="true", zorder=0, alpha=0.1))
        if nconc and metabolites_to_plot[1] in nconc.keys():
            my_nconcs = nconc[metabolites_to_plot[1]]
            ax.add_patch(Rectangle((ax.get_xlim()[0], my_nconcs['min']), ax.get_xlim()[1]+abs(0-ax.get_xlim()[0]), (my_nconcs['max']-my_nconcs['min']), facecolor="lightgreen", fill="true", zorder=0, alpha=0.1))

        plt.title(f"Sample Distribution for {capitalizeChemical(metabolites_to_plot[0])} / {capitalizeChemical(metabolites_to_plot[1])}", loc='left', fontsize=20)
        plt.xlabel(metabolites_to_plot[0] + " (" + unit_map[metabolites_to_plot[0]]+ ")")
        plt.ylabel(metabolites_to_plot[1] + " (" + unit_map[metabolites_to_plot[1]]+ ")") 
        plt.tight_layout()

        bytes_image = io.BytesIO()
        FigureCanvas(f).print_png(bytes_image)
        plt.close('all')

        return bytes_image

    def generateHistPlot(self, data_frames: list, unit_map: dict, phenotype_map: dict, nconc: dict, plot_bins:int=None, phenotypes:list=None, histstyle:str="step|fill"):
        """
        Generates a histogram.

        :param data_to_plot: pandas DataFrame containing the data to plot
        :param nconc: Normal concentration data (min/max) for the metabolites
        :param plot_bins: Number of bins to use for the histogram
        :returns BytesIO object containing the plot image
        """
        if phenotypes and phenotype_map and len(phenotype_map) > 0:
            for df in data_frames:
                for study in phenotype_map.keys():
                    for pf in phenotype_map[study].keys():
                        for src in phenotype_map[study][pf].keys():
                            df.loc[src, pf]= phenotype_map[study][pf][src] if phenotypes else "Unknown"

        data_frames = pd.concat(data_frames, ignore_index=True)

        if phenotypes and phenotype_map and len(phenotype_map) > 0:
            pheno_hue = data_frames[["study", phenotypes[0]]].dropna().apply(tuple, axis=1)
            logging.info(pheno_hue)


        f, ax = plt.subplots(figsize=(11, 9))
        metabolites_to_plot = list(unit_map.keys())
        if len(metabolites_to_plot) < 1:
            return "nope"

        if not plot_bins:
            bins=30
        else:
            bins = plot_bins

        hist_split = histstyle.split("|")
        hist_element = hist_split[0]
        hist_fill = True if hist_split[1] == "fill" else False
        
        if phenotypes and phenotype_map and len(phenotype_map) > 0:
            sns.histplot(data=data_frames, x=metabolites_to_plot[0], palette="muted", hue=pheno_hue, bins=int(bins),
             alpha=0.6, zorder=1, stat="probability", common_norm=False, element=hist_element, multiple="layer", fill=hist_fill)
        else:
            sns.histplot(data=data_frames, x=metabolites_to_plot[0], palette="Blues", hue="study", bins=int(bins),
             alpha=0.6, zorder=1, stat="probability", common_norm=False, element=hist_element, multiple="layer", fill=hist_fill)

        if nconc and metabolites_to_plot[0] in nconc.keys():
            nconc_patch = nconc[metabolites_to_plot[0]]
            ax.add_patch(Rectangle((nconc_patch['min'], 0), (nconc_patch['max']-nconc_patch['min']), ax.get_ylim()[1], facecolor="lightgreen", fill="true", zorder=0, alpha=0.1))

            
        plt.title(f"Concentration Histogram for {capitalizeChemical(metabolites_to_plot[0])}", loc='left', fontsize=20)
        #plt.legend(title=("Study" if not phenotypes else "Study - Phenotype"))
        plt.xlabel(f"{metabolites_to_plot[0]} concentration (" + unit_map[metabolites_to_plot[0]] + ")")
        plt.ylabel("Percentage (in respective study)") 
        plt.tight_layout()

        bytes_image = io.BytesIO()
        FigureCanvas(f).print_png(bytes_image)
        plt.close('all')

        return bytes_image

    def generateCorrelationPlot(self, data_frames: list) -> io.BytesIO:
        """
        Generates a heatmap.

        :param data_to_plot: pandas DataFrame containing the data to plot
        :returns BytesIO object containing the plot image
        """
        data_frames = pd.concat(data_frames, ignore_index=True)
        df_without_study = data_frames.drop(['study'], axis=1)
        correlation_map = df_without_study.astype(float).corr(method='pearson')

        f, _ = plt.subplots(figsize=(11, 9))
        cmap = sns.diverging_palette(250, 10, as_cmap=True)
        #cmap = sns.color_palette("coolwarm", as_cmap=True)
        mask = np.zeros_like(correlation_map, dtype=bool)
        mask[np.triu_indices_from(mask)] = True
        
        sns.heatmap(correlation_map, mask=mask, cmap=cmap, center=0, square=True, linewidths=0.5)
        plt.title("Correlation map for selected samples", loc='left', fontsize=20)
        plt.tight_layout()

        bytes_image = io.BytesIO()
        FigureCanvas(f).print_png(bytes_image)
        plt.close('all')

        return bytes_image
